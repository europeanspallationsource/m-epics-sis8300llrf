###########
# Reference compensation monitor values

record(ai, "$(P)$(R)RefCompCtrLin-I") {
    field(DESC, "Controller input signal I")
    info(DESCRIPTION, "Controller input signal I")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) CtrlInIAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefCompCtrLin-Q") {
    field(DESC, "Controller input signal Q")
    info(DESCRIPTION, "Controller input signal Q")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) CtrlInQAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefComp-I") {
    field(DESC, "Reference Line signal I")
    info(DESCRIPTION, "Reference Line signal I")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) RefIAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefComp-Q") {
    field(DESC, "Refrence Line signal Q")
    info(DESCRIPTION, "Refrence Line signal Q")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) RefQAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefCompCtrLinMag") {
    field(DESC, "Controller input signal mag")
    info(DESCRIPTION, "Controller input signal mag")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) CtrlInMagAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefCompCtrLinAng") {
    field(DESC, "Controller input signal ang")
    info(DESCRIPTION, "Controller input signal ang")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) CtrlInAngAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefCompMag") {
    field(DESC, "Reference Line signal mag")
    info(DESCRIPTION, "Reference Line signal mag")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) RefMagAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}

record(ai, "$(P)$(R)RefCompAng") {
    field(DESC, "Refrence Line signal ang")
    info(DESCRIPTION, "Refrence Line signal ang")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) RefAngAvg")
    field(SCAN, "I/O Intr")
    field(PREC, "11")
}


##########
# reference compensation monitor channel properties/settings


record(ao, "$(P)$(R)RefCompAvgPos") {
    field(DESC, "Position of averaging window")
    info(DESCRIPTION, "Position of averaging window")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).refcomp, 0) AveragePos")
    field(PREC, "0")
    field(VAL, "0")
    field(PINI, "YES")
    field(DRVL, "0")
    field(DRVH, "8388608")
}

record(ai, "$(P)$(R)RefCompAvgPos-RB") {
    field(DESC, "Position of averaging window RBV")
    info(DESCRIPTION, "Position of averaging window RBV")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) AveragePos")
    field(SCAN, "I/O Intr")
    field(PREC, "0")
}

record(ao, "$(P)$(R)RefCompAvgWid") {
    field(DESC, "Averaging window width")
    info(DESCRIPTION, "Averaging window width")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).refcomp, 0) AverageWidth")
    field(PREC, "0")
    field(VAL, "1")
    field(PINI, "YES")
    field(DRVL, "1")
    field(DRVH, "8388608")
}

record(ai, "$(P)$(R)RefCompAvgWid-RB") {
    field(DESC, "Averaging window width RBV")
    info(DESCRIPTION, "Averaging window width RBV")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).refcomp, 0) AverageWidth")
    field(SCAN, "I/O Intr")
    field(PREC, "0")
}
