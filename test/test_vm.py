#!/usr/bin/env python3
from random import randint, random

from helper import change_state, check_readback, float_to_fixed, fixed_to_float,\
                   read_reg, check_readback_float
from helper import caget_assert, caput_assert

class TestVM:
    def test_bo_readbacks(self, board, prefixdig, prefixrtm):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixrtm + ":VMInvIEn", 1)
        assert check_readback(prefixrtm + ":VMInvIEn", 0)

        assert check_readback(prefixrtm + ":VMInvQEn", 1)
        assert check_readback(prefixrtm + ":VMInvQEn", 0)

        assert check_readback(prefixrtm + ":VMSwapIQEn", 1)
        assert check_readback(prefixrtm + ":VMSwapIQEn", 0)

        assert check_readback(prefixrtm + ":VMMagLimEn", 1)
        assert check_readback(prefixrtm + ":VMMagLimEn", 0)

        assert check_readback(prefixrtm + ":VMPredistEn", 1)
        assert check_readback(prefixrtm + ":VMPredistEn", 0)

    def test_ao_readbacks(self, board, prefixdig, prefixrtm):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)


        assert check_readback_float(prefixrtm + ":VMMagLim", 16, 16, 0)
        assert check_readback_float(prefixrtm + ":VMPredistDCOI", 1, 15, 1)
        assert check_readback_float(prefixrtm + ":VMPredistDCOQ", 1, 15, 1)
        assert check_readback_float(prefixrtm + ":VMPredistRC00", 2, 14, 1)
        assert check_readback_float(prefixrtm + ":VMPredistRC01", 2, 14, 1)
        assert check_readback_float(prefixrtm + ":VMPredistRC10", 2, 14, 1)
        assert check_readback_float(prefixrtm + ":VMPredistRC11", 2, 14, 1)


    def test_output_en(self, board, prefixdig,  prefixrtm):
        # Check current main/secondary status of board for later reversion if required
        latchSecondary = 0
        main = caget_assert(prefixdig + ":Main-RB")
        if main == 0:
            latchSecondary = 1
            caput_assert(prefixdig + ":Main.DISP", 0)
            caput_assert(prefixdig + ":Main", 1)
        res = caput_assert(prefixrtm + ":VM-En", 1)
        assert res == 1
        reg = read_reg(board, "0x12F")
        assert reg == "0x700"

        # Revert board to its original "main/secondary" status
        if latchSecondary:
             caput_assert(prefixdig + ":Main", 0)
             caput_assert(prefixdig + ":Main.DISP", 1)
             latchSecondary = 0

    def test_output_dis(self, board, prefixdig, prefixrtm):
        # Check current main/secondary status of board for later reversion if required
        latchSecondary = 0
        main = caget_assert(prefixdig + ":Main-RB")
        if main == 0:
            latchSecondary = 1
            caput_assert(prefixdig + ":Main.DISP", 0)
            caput_assert(prefixdig + ":Main", 1)
        res = caput_assert(prefixrtm + ":VM-En", 0)
        assert res == 1
        reg = read_reg(board, "0x12F")
        assert reg == "0x600"

        # Revert board to its original "main/secondary" status
        if latchSecondary:
             caput_assert(prefixdig + ":Main", 0)
             caput_assert(prefixdig + ":Main.DISP", 1)
             latchSecondary = 0
