from math import nan
from random import randint
from time import sleep

import pytest
import numpy as np
from helper import caget_assert, caput_assert
from helper import generate_csv, generate_csv_rand, generate_csv_unordered,\
                   PREC, PREC_SLOPE, PREC_OFFSET, change_state, sim_bp_trig

def test_set_non_linear_calib(prefixdig):
    for ch in range(0,10):
        caput_assert(prefixdig + (":AI%d-CalLin") % ch, 0)

    for state in ["RESET", "INIT", "ON"]:
        assert change_state(prefixdig, state)


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_non_linear(prefixdig, ch, file_name, max_elem, board):
# generate random files and set them on channels
    _, _, egu, raw = generate_csv(file_name, max_elem)
    caput_assert(prefixdig + (":AI%d-CalLin") % ch, 0)
    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)

# run an acquisition
    sim_bp_trig(board)
    sleep(0.1)
# check the PVs
    egu_rbv = caget_assert(prefixdig + (":AI%d-CalEGU") % ch)
    raw_rbv = caget_assert(prefixdig + (":AI%d-CalRaw") % ch)
    assert np.array_equal(egu, egu_rbv)
    assert np.array_equal(raw, raw_rbv)

    ## check if reset is working
    # reset calib
    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, "")
    # check if the arrays are empty
    sleep(0.1)

    egu_rbv = caget_assert(prefixdig + (":AI%d-CalEGU") % ch)
    raw_rbv = caget_assert(prefixdig + (":AI%d-CalRaw") % ch)
    assert np.array_equal([], egu_rbv)
    assert np.array_equal([], raw_rbv)


def test_set_linear_calib(prefixdig):
    for ch in range(0,10):
        caput_assert(prefixdig + (":AI%d-CalLin") % ch, 1)

    for state in ["RESET", "INIT"]:
        assert change_state(prefixdig, state)


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_csv(prefixdig, ch,  file_name, max_elem):
    """
    Make a test for a random csv file, verifying if the slope and offset were
    correctly calculated
    prefixdig: PV prefixdig
    file_name: name of csv file to be generated
    max_elem: number maximum of elements to be generated
    """
    elem = randint(2, max_elem) if max_elem > 1 else 1
    (slope, offset, _, _) = generate_csv(file_name, elem)

    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
    sleep(0.1)

    slope_IOC = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
    assert slope_IOC != nan

    offset_IOC = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)
    assert offset_IOC != nan

    print("slope ioc ", slope_IOC)
    print("offset ioc ", offset_IOC)
    diff_slope = abs(slope - slope_IOC)
    diff_offset = abs(offset - offset_IOC)

    assert diff_slope < 10**-(PREC_SLOPE-1)
    assert diff_offset < 10**-(PREC_OFFSET-1)

    ## check if reset is working
    # reset calib
    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, "")
    # check if the arrays are empty
    sleep(0.1)

    egu_rbv = caget_assert(prefixdig + (":AI%d-CalEGU") % ch)
    raw_rbv = caget_assert(prefixdig + (":AI%d-CalRaw") % ch)
    assert np.array_equal([], egu_rbv)
    assert np.array_equal([], raw_rbv)


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_fitted_line(prefixdig, ch, file_name, max_elem):
    """
    check if fitted line is right
    """
    generate_csv_rand(file_name, max_elem)

    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
    sleep(0.1)

    # check if the number of elements is correct
    n_elem = caget_assert(prefixdig + (":AI%d-CalRaw.NORD") % ch)
    assert n_elem == max_elem

    # check if the last element on fitted line is correct
    slope = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
    assert slope != nan

    offset = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)
    assert offset != nan

    elems = caget_assert(prefixdig + (":AI%d-CalFit") % ch)
    assert len(elems) == max_elem

    last_elem = elems[-1]

    elems_dig = caget_assert(prefixdig + (":AI%d-CalRaw") % ch)
    assert len(elems_dig) == max_elem

    last_elem_dig = elems_dig[-1]

    diff = abs(last_elem - (last_elem_dig*slope + offset))

    assert diff < 10**-(PREC-1)


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_one_line(prefixdig, ch, file_name):
    generate_csv(file_name, 1)

    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
    sleep(0.1)

    slope = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
    assert slope != nan

    offset = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)
    assert offset != nan

    assert slope == 1 and offset == 0


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_unorderd_csv(prefixdig, ch, file_name, max_elem):
    generate_csv_unordered(file_name, max_elem)

    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
    sleep(0.1)

    stat = caget_assert(prefixdig + (":AI%d-CalCSV.STAT") % ch)
    sevr = caget_assert(prefixdig + (":AI%d-CalCSV.SEVR") % ch)

    slope = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
    assert slope != nan

    offset = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)
    assert offset != nan

    assert stat == 2 and sevr == 3 and slope == 1 and offset == 0


@pytest.mark.parametrize("ch", list(range(0,10)))
def test_last_lines_blank(prefixdig, ch, file_name, max_elem):
    (slope, offset, _, _) = generate_csv(file_name, max_elem)

    # add n blank lines at the end
    with open(file_name, "a") as f:
        f.seek(0, 2)
        for i in range(randint(1, max_elem)):
            f.write("\n")

    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
    sleep(0.1)

    slope_IOC = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
    assert slope != nan

    offset_IOC = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)
    assert offset != nan

    diff_slope = abs(slope - slope_IOC)
    diff_offset = abs(offset - offset_IOC)
    print("slope ioc ", slope_IOC)
    print("offset ioc ", offset_IOC)

    assert diff_slope < 10**-(PREC_SLOPE-1)
    assert diff_offset < 10**-(PREC_OFFSET-1)
