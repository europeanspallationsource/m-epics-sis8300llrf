from random import randint
from time import sleep

import numpy as np
from helper import caput_assert, caget_assert, ca
from helper import change_state, sim_bp_trig, check_readback
from test_downsampled import get_max_acq, get_max_dec

INTERPVNAMES = ["PIErr", "CavRot", "CavFilt", "PIOut", "FWOut", "ILCtrl"]


class TestInternalChannels:
    def test_params (self, prefix, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # test params for all channels
        for ch in INTERPVNAMES:
            assert check_readback(prefixdig + (":IntCh%s-En" % ch), 1)
            if ch != "ILCtrl":
                assert check_readback(prefixdig + (":IntCh%s-DecF" % ch), 1)
            # number of samples
            smnm = randint(1, get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":IntCh%s-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":IntCh%s-SmNm-RB" % ch))
            if ch != "ILCtrl":
                #decimation
                assert check_readback(prefixdig + (":IntCh%s-DecEn" % ch), 1)
                max_dec = get_max_dec(prefix, prefixdig, smnm_rbv)
                dec_fac = randint(1, max_dec)
                assert check_readback(prefixdig + (":IntCh%s-DecF" % ch), dec_fac)
                assert check_readback(prefixdig + (":IntCh%s-DecEn" % ch), 0)

    """
    Test limits for number of acquisitions and decimation factor
    """
    def test_limits(self, prefix, prefixdig):
        # test params for all channels
        for ch in INTERPVNAMES:
            # check limit for number of samples
            if ch != "ILCtrl":
                assert check_readback(prefixdig + (":IntCh%s-DecF" % ch), 1)
            smnm = randint(get_max_acq(prefix, prefixdig, 1), 10*get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":IntCh%s-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":IntCh%s-SmNm-RB" % ch))
            assert smnm_rbv <= get_max_acq(prefix, prefixdig, 1)

            #ILCtrl doesn't have decimation
            if ch != "ILCtrl":
                # back to lower value to number of samples
                assert check_readback(prefixdig + (":IntCh%s-SmNm" % ch), 32)
                sleep(0.1)

                #check limits for decimation factor
                max_dec = get_max_dec(prefix, prefixdig, 32)
                dec_factor = randint(max_dec, 10*max_dec)
                caput_assert(prefixdig + (":IntCh%s-DecF" % ch), dec_factor)
                sleep(0.1)
                dec_factor_rbv = caget_assert(prefixdig + (":IntCh%s-DecF-RB" % ch))

                assert dec_factor_rbv <= max_dec
                # return decimation to lower value and smnm to higher
                caput_assert(prefixdig + (":IntCh%s-DecF" % ch), 1)
                sleep(0.1)

            caput_assert(prefixdig + (":IntCh%s-SmNm" % ch), get_max_acq(prefix, prefixdig, 1))
            sleep(0.1)



    """
    Test to check if after a simulated pulse any data was acquired
    """
    def test_acq(self, prefixdig, board):
        for ch in INTERPVNAMES:
            if ch != "ILCtrl":
                # disable decimation
                caput_assert(prefixdig + (":IntCh%s-DecEn" % ch), 0)
            # enable channel
            caput_assert(prefixdig + (":IntCh%s-En") % ch, 1)
            # enable transfer channel
            caput_assert(prefixdig + (":IntCh%s-EnTransf") % ch, 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)
        # run a pulse
        sim_bp_trig(board)

        sleep(2)
        for ch in INTERPVNAMES:
            # get read the value
            vals_imag = caget_assert(prefixdig + (":IntCh%s-Cmp0") % ch)
            vals_qang = caget_assert(prefixdig + (":IntCh%s-Cmp1") % ch)

            vals_imag_avg = np.average(vals_imag)
            vals_qang_avg = np.average(vals_qang)

            # check if the values are in the correct interval
            assert vals_imag_avg < 100 and vals_imag_avg > -100
            assert vals_qang_avg < 100 and vals_qang_avg > -100

            # checking that there are values on the waveforms - this is
            # not true for channels PIOUT and FWOUT
            if ch != INTERPVNAMES[3] and ch != INTERPVNAMES[4]:
                assert vals_imag_avg != 0
                assert vals_qang_avg != 0

    """
    Test X Axis for all channels
    """
    def test_xax_ch(self, prefix, prefixdig, board):

        for ch in INTERPVNAMES:
            for state in ["RESET", "INIT"]:
                assert change_state(prefixdig, state)
            if ch != "ILCtrl":
                assert check_readback(prefixdig + (":IntCh%s-DecF" % ch), 1)
            smnm = randint(1, get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":IntCh%s-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":IntCh%s-SmNm-RB" % ch))

            if ch != "ILCtrl":
                assert check_readback(prefixdig + (":IntCh%s-DecEn" % ch), 1)
                max_dec = get_max_dec(prefix, prefixdig, smnm_rbv)
                dec_fac = randint(1, max_dec)
                assert check_readback(prefixdig + (":IntCh%s-DecF" % ch), dec_fac)
            else:
                dec_fac = 1

            f_samp = caget_assert(prefix + ":FreqSampling")
            near_iq_n = caget_assert(prefixdig + ":IQSmplNearIQN-RB")

            assert change_state(prefixdig, "ON")

            # run a pulse
            sim_bp_trig(board)

            ca.poll(5)
            # check first position
            vals = caget_assert(prefixdig + (":IntCh%s-XAxis") % ch, param_timeout=10)
            assert  vals[0] == 0

            smnm_acq = caget_assert(prefixdig + (":IntCh%s-AcqSm") % ch)
            # check a random position
            maxi = (smnm_acq//dec_fac)  - 1
            if maxi > 1 :
                pos = randint(1, maxi)
            else:
                pos = 1

            assert '%.6f' % vals[pos] == '%.6f' % ((pos*dec_fac) * (1/(f_samp * 1000)*near_iq_n))

            # check last position
            pos = int(maxi) - 1
            assert '%.6f' % vals[pos] == '%.6f' % ((pos*dec_fac) * (1/(f_samp * 1000)*near_iq_n))


#TODO: implement correctly using DAQFMT
#    def test_pi_error_rms(self, prefixdig, board):
#        # disable decimation
#        caput_assert(prefixdig + (":IntCh%s-DecEn" % INTERPVNAMES[0]), 0)
#        # enable channel
#        caput_assert(prefixdig + (":IntCh%s-En") % INTERPVNAMES[0], 1)
#        # enable transfer channel
#        caput_assert(prefixdig + (":IntCh%s-EnTransf") % INTERPVNAMES[0], 1)
#
#        for state in ["RESET", "INIT", "ON"]:
#            assert change_state(prefixdig, state)
#
#        run_evr_pulses(prefixdig, 10)
#
#        ca.poll(1)
#        # all the RMS values should be different from 0
#        for suf in ["-RMS-I", "-RMS-Q", "-RMS-AVERAGE-I", "-RMS-AVERAGE-Q",
#                "-RMS-MAX-I", "-RMS-MAX-Q"]:
#            assert (caget_assert(prefixdig+":"+INTERPVNAMES[0]+suf) is not None
#                and caget_assert(prefixdig+":"+INTERPVNAMES[0]+suf) != 0)
#
#        assert caget_assert(prefixdig+":"+INTERPVNAMES[0]+"-RMS-PULSECNT") == 10
#
#        # reset and check the values
#        caput_assert(prefixdig+":"+INTERPVNAMES[0]+"-RMS-RESET", 1)
#        caput_assert(prefixdig+":"+INTERPVNAMES[0]+"-RMS-RESET", 0)
#
#        for suf in ["-RMS-AVERAGE-I", "-RMS-AVERAGE-Q",
#                "-RMS-MAX-I", "-RMS-MAX-Q", "-RMS-PULSECNT"]:
#            assert caget_assert(prefixdig+":"+INTERPVNAMES[0]+suf) == 0

    # check if values from internal and downsampled are different
    def test_inter_down(self, prefixdig, board):
        for ch in range(0, 6):
            # disable decimation
            caput_assert(prefixdig + (":Dwn%d-DecEn" % ch), 0)
            # enable channel
            caput_assert(prefixdig + (":Dwn%d-En") % ch, 1)
            # enable transfer channel
            caput_assert(prefixdig + (":Dwn%d-EnTransf") % ch, 1)
            if INTERPVNAMES[ch] != "ILCtrl":
                # disable decimation
                caput_assert(prefixdig + (":IntCh%s-DecEn" % INTERPVNAMES[ch]), 0)
            # enable channel
            caput_assert(prefixdig + (":IntCh%s-En") % INTERPVNAMES[ch], 1)
            # enable transfer channel
            caput_assert(prefixdig + (":IntCh%s-EnTransf") % INTERPVNAMES[ch], 1)

        # test for the 3 formats
        for fmt in range(3):
            for ch in range(0, 6):
                # change DAQ format
                caput_assert(prefixdig + (":Dwn%d-Fmt") % ch, fmt)

            for state in ["RESET", "INIT", "ON"]:
                assert change_state(prefixdig, state)
            # run a pulse
            sim_bp_trig(board)

            sleep(0.1)
            for ch in range(0, 6):
                # get read the value
                vals_cmp0_down = caget_assert(prefixdig + (":Dwn%d-Cmp0") % ch)
                vals_cmp0_inter = caget_assert(prefixdig + (":IntCh%s-Cmp0") % INTERPVNAMES[ch])
                vals_cmp1_down = caget_assert(prefixdig + (":Dwn%d-Cmp1") % ch)
                vals_cmp1_inter = caget_assert(prefixdig + (":IntCh%s-Cmp1") % INTERPVNAMES[ch])

                assert np.average(vals_cmp0_down) != np.average(vals_cmp0_inter)
                assert np.average(vals_cmp1_down) != np.average(vals_cmp1_inter)

    """
    Check if PIERR has different values using IQ or MA
    """
    def test_pierr_daqfmt(self, prefixdig, board):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)
        assert check_readback(prefixdig + (":IntCh%s-Fmt" % INTERPVNAMES[0]), 0) #MAG/ANG
        assert check_readback(prefixdig + (":IntCh%s-Fmt" % INTERPVNAMES[0]), 1) #IQ

        assert change_state(prefixdig, "ON")

        # run a pulse
        sim_bp_trig(board)

        ca.poll(2)
        vals_pierr_i = caget_assert(prefixdig + (":IntCh%s-Cmp0") % INTERPVNAMES[0])
        vals_pierr_q = caget_assert(prefixdig + (":IntCh%s-Cmp1") % INTERPVNAMES[0])
        assert np.average(vals_pierr_i) != 0
        assert np.average(vals_pierr_q) != 0

        # back to MAG/ANG
        caput_assert(prefixdig + (":IntCh%s-Fmt" % INTERPVNAMES[0]), 0) #MAG/ANG
        # run a pulse
        sim_bp_trig(board)

        ca.poll(2)
        vals_pierr_mag = caget_assert(prefixdig + (":IntCh%s-Cmp0") % INTERPVNAMES[0])
        vals_pierr_ang = caget_assert(prefixdig + (":IntCh%s-Cmp1") % INTERPVNAMES[0])
        assert np.average(vals_pierr_mag) != 0
        assert np.average(vals_pierr_ang) != 0


        assert np.average(vals_pierr_i) != np.average(vals_pierr_mag)
        assert np.average(vals_pierr_q) != np.average(vals_pierr_ang)

    def test_normal_behavior(self, prefixdig, board):
        # check if the system keep working correctly
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        sim_bp_trig(board)
        sim_bp_trig(board)
        ca.poll(1)
        pulsecnt = caget_assert(prefixdig + ":PulseDoneCnt")
        assert pulsecnt  == 2
