#!/usr/bin/env python3
import pytest
from random import randint

from helper import change_state, sim_bp_trig
from helper import caget_assert, caput_assert


class TestXAxis:
    @pytest.mark.parametrize("ch", list(range(0,10)))
    def test_xax_ch(self, prefix, prefixdig, ch):
        """
        Test X Axis for an specific channel
        """
        smnm = caget_assert(prefixdig + ":ChGrpAI-SmNm-RB")
        f_samp = caget_assert(prefix + ":FreqSampling")

        # check first position
        vals = caget_assert(prefixdig + (":AI%d-XAxis") % ch)
        assert '%.9f' % vals[0] == '%.9f' % (0)

        # check a random position
        maxi = smnm - 1
        if maxi > 1 :
            pos = randint(1, maxi)
        else:
            pos = 1

        assert '%.6f' % vals[pos] == '%.6f' % (pos * (1/(f_samp * 1000)))
