import pytest
from helper import BASE_BOARD, SIS8300DRV_RTM_ATT_VM, get_mods_vers, MAXELEM, FILENAME,\
                   PREFIXDIG, PREFIXRTM


def pytest_addoption(parser):
    parser.addoption("--board", required="True")
    parser.addoption("--prefix", required="True")
    parser.addoption("--prefixdig")
    parser.addoption("--prefixrtm")
    parser.addoption("--file")
    parser.addoption("--max")

def pytest_generate_tests(metafunc):
    if "board" in metafunc.fixturenames:
        b = metafunc.config.getoption("board")
        if b is not None:
            metafunc.parametrize("board", [BASE_BOARD + '-' + b])

    if "prefix" in metafunc.fixturenames:
        p = metafunc.config.getoption("prefix")
        if p is not None:
            metafunc.parametrize("prefix", [p])

    if "prefixdig" in metafunc.fixturenames:
        p = metafunc.config.getoption("prefix")
        pd = metafunc.config.getoption("prefixdig")
        if pd is not None:
            metafunc.parametrize("prefixdig", [pd])
        else:
            metafunc.parametrize("prefixdig", [p + PREFIXDIG])

    if "prefixrtm" in metafunc.fixturenames:
        p = metafunc.config.getoption("prefix")
        pr = metafunc.config.getoption("prefixrtm")
        if pr is not None:
            metafunc.parametrize("prefixrtm", [pr])
        else:
            metafunc.parametrize("prefixrtm", [p + PREFIXRTM])

    if "file_name" in metafunc.fixturenames:
        f = metafunc.config.getoption("file")
        if f is not None:
            metafunc.parametrize("file_name", [f])
        else:
            metafunc.parametrize("file_name", [FILENAME])

    if "max_elem" in metafunc.fixturenames:
        m = metafunc.config.getoption("max")
        if m is not None:
            m = int(m)
            metafunc.parametrize("max_elem", [m])
        else:
            metafunc.parametrize("max_elem", [MAXELEM])

def pytest_report_header(config):
    return get_mods_vers()
