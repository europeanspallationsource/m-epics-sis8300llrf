from helper import caget_assert
from helper import change_state, check_readback

class TestTableMode:
    def test_table_mode(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":FwTblS-FF-Mode", 1)
        assert caget_assert(prefixdig + ":FwTblS-FF-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":FwTblS-FF-Mode.SEVR") == 0
        assert check_readback(prefixdig + ":FwTblS-FF-Mode", 0)
        assert caget_assert(prefixdig + ":FwTblS-FF-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":FwTblS-FF-Mode.SEVR") == 0
        assert check_readback(prefixdig + ":FwTblS-SP-Mode", 1)
        assert caget_assert(prefixdig + ":FwTblS-SP-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":FwTblS-SP-Mode.SEVR") == 0
        assert check_readback(prefixdig + ":FwTblS-SP-Mode", 0)
        assert caget_assert(prefixdig + ":FwTblS-SP-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":FwTblS-SP-Mode.SEVR") == 0
