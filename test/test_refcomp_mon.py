from random import randint

from helper import check_readback, change_state, sim_bp_trig
from helper import caget_assert

class TestRefCompMonitor:
    def test_basic(self, prefixdig, board):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        #TODO improve this values
        avgpos = randint(1,100)
        avgwidth = randint(100, 10000)

        check_readback(prefixdig + ":RefCompAvgPos", avgpos)
        check_readback(prefixdig + ":RefCompAvgWid", avgwidth)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        assert caget_assert(prefixdig + ":RefCompCtrLin-I") != 0 or \
            caget_assert(prefixdig + ":RefCompCtrLin-Q") != 0 or \
            caget_assert(prefixdig + ":RefComp-I") != 0 or \
            caget_assert(prefixdig + ":RefComp-Q") != 0

        assert caget_assert(prefixdig + ":RefCompCtrLinMag") != 0 or \
            caget_assert(prefixdig + ":RefCompCtrLinAng") != 0 or \
            caget_assert(prefixdig + ":RefCompMag") != 0 or \
            caget_assert(prefixdig + ":RefCompAng") != 0
