#!/usr/bin/env python3
from time import sleep

from helper import change_state, sim_bp_trig
from helper import caget_assert, caput_assert
from test_scaling import test_set_linear_calib, test_csv

class TestSignalMonitoring:
    # Set a threshold and check if the alarms will work
    def test_alarms(self, prefixdig, board):
        # Over the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        sleep(0.1)
        # check the alarms
        for ch in range(10):
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 1
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 1

        # Bellow the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 1)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        sleep(0.1)
        # check the alarms
        for ch in range(10):
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 1
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 1


    # Set a threshold and check if the alarms will work
    def test_alarms_latch(self, prefixdig, board):
        # first test without resetting
        # Over the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        sleep(0.1)
        # check the alarms
        for ch in range(10):
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 1
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 1

        # Bellow the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 1)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        # run 2 pulses to apply setup
        sim_bp_trig(board)
        sleep(0.1)
        sim_bp_trig(board)
        sleep(0.1)

        # check if the alarms are disabled
        for ch in range(10):
            print("Channel ", ch)
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 0
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 0

        # second test resetting
        # Over the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        sleep(0.1)
        # check the alarms
        for ch in range(10):
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 1
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 1

        # Bellow the threshold
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 2)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 1)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 1)

        # reset to apply changes
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        sim_bp_trig(board)
        sleep(0.1)

        # check if the alarms are disabled
        for ch in range(10):
            print("Channel ", ch)
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmStat") == 0
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckStat") == 0



    def test_return_orig_state(self, prefixdig):
        # return for origina state
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStartEvnt", 3)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonStopEvnt", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonMagLim", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAlrmCnd", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonPMSEn", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonIlckEn", 0)


# set parameters for average window and check if the read value is != 0
    def test_mag_avg(self, prefixdig, board):
        for ch in range(10):
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAvgPos", 0)
            caput_assert(prefixdig + ":AI" + str(ch) + "-SMonAvgWid", 8000) # TODO: check this


        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)

        sleep(0.1)
        # check the alarms
        for ch in range(10):
            # TODO: this could fail if the averagin has not finished properly
            # during last pulse (on firmware)
            assert caget_assert(prefixdig + ":AI" + str(ch) + "-SMonMagAvg") != 0

    # check if the calibration on raw channels are applied to monitoring ones
    def test_calibration(self, prefixdig, board, file_name, max_elem):
        # load linear calibration
        test_set_linear_calib(prefixdig)

        # load a random csv
        for ch in range(10):
            test_csv(prefixdig, ch, file_name, max_elem)


        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board)


        sleep(0.1)
        for ch in range(10):
            slope = caget_assert(prefixdig + (":AI%d-LCvF-RB") % ch)
            offset = caget_assert(prefixdig + (":AI%d-LCvO-RB") % ch)

            magminmax = caget_assert(prefixdig + (":AI%d-SMonMagMinMax") % ch)
            magavg = caget_assert(prefixdig + (":AI%d-SMonMagAvg") % ch)

            magminmaxraw = caget_assert(prefixdig + (":AI%d-SMonMagMinMaxRaw") % ch)
            magavgraw = caget_assert(prefixdig + (":AI%d-SMonMagAvgRaw") % ch)

            assert (magminmaxraw*slope + offset) == magminmax
            assert (magavgraw*slope + offset) == magavg
