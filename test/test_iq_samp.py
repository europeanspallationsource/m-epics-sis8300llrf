#!/usr/bin/env python3
import pytest
from random import randint, random

from helper import change_state, check_readback, check_readback_float
from helper import caget_assert, caput_assert

class TestIQSampling:
    def test_cav_inp(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":IQSmplCavInDlyEn", 1)
        assert check_readback(prefixdig + ":IQSmplCavInDlyEn", 0)

        assert check_readback(prefixdig + ":IQSmplCavInDly", randint(0, 63))

    def test_iq_ang_off(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":IQSmplAngOffsetEn", 1)
        assert check_readback(prefixdig + ":IQSmplAngOffsetEn", 0)

        assert check_readback_float(prefixdig + ":IQSmplAngOffset", 16, 16, 1)
