#!/usr/bin/env python3
from random import randint
from time import sleep

from helper import caput_assert, caget_assert, ca
from helper import change_state, check_readback, sim_bp_trig
import numpy as np

MAXINTERVAL = 65 #ms

def get_max_acq(prefix, prefixdig, dec_fac):
    fsamp = caget_assert(prefix + ":FreqSampling")
    neariqn = caget_assert(prefixdig + ":IQSmplNearIQN-RB")

    faclimit = 1/(fsamp*1000)*neariqn
    return int(MAXINTERVAL/(faclimit*dec_fac))

def get_max_dec(prefix, prefixdig, smnm):
    fsamp = caget_assert(prefix + ":FreqSampling")
    neariqn = caget_assert(prefixdig + ":IQSmplNearIQN-RB")

    faclimit = 1/(fsamp*1000)*neariqn
    return int(MAXINTERVAL/(faclimit*smnm))



class TestDownSampledChannels:
    def test_params (self, prefix, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # test params for all channels
        for ch in range(0,10):
            assert check_readback(prefixdig + (":Dwn%d-En" % ch), 1)
            assert check_readback(prefixdig + (":Dwn%d-DecF" % ch), 1)
            # number of samples
            smnm = randint(1, get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":Dwn%d-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":Dwn%d-SmNm-RB" % ch))
            assert smnm == int(smnm_rbv)
            #decimation
            assert check_readback(prefixdig + (":Dwn%d-DecEn" % ch), 1)
            max_dec = get_max_dec(prefix, prefixdig, smnm_rbv)
            dec_factor = randint(1, max_dec)
            assert check_readback(prefixdig + (":Dwn%d-DecF" % ch), dec_factor)
            assert check_readback(prefixdig + (":Dwn%d-DecEn" % ch), 0)
            # DAQFMT
            assert check_readback(prefixdig + (":Dwn%d-Fmt" % ch), 1) #IQ
            assert check_readback(prefixdig + (":Dwn%d-Fmt" % ch), 2) #DC
            assert check_readback(prefixdig + (":Dwn%d-Fmt" % ch), 0) #MAG/ANG

    """
    Test limits for number of acquisitions and decimation factor
    """
    def test_limits(self, prefix,  prefixdig):
        # test params for all channels
        for ch in range(0,10):
            # check limit for number of samples
            assert check_readback(prefixdig + (":Dwn%d-DecF" % ch), 1)
            smnm = randint(get_max_acq(prefix, prefixdig, 1), 10*get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":Dwn%d-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":Dwn%d-SmNm-RB" % ch))
            assert smnm_rbv <= get_max_acq(prefix, prefixdig, 1)

            # back to lower value to number of samples
            assert check_readback(prefixdig + (":Dwn%d-SmNm" % ch), 32)
            sleep(0.1)

            #check limits for decimation factor
            max_dec = get_max_dec(prefix, prefixdig, 32)
            dec_factor = randint(max_dec, 10*max_dec)
            caput_assert(prefixdig + (":Dwn%d-DecF" % ch), dec_factor)
            sleep(0.1)
            dec_factor_rbv = caget_assert(prefixdig + (":Dwn%d-DecF-RB" % ch))

            assert dec_factor_rbv <= max_dec

            # return decimation to lower value and smnm to higher
            caput_assert(prefixdig + (":Dwn%d-DecF" % ch), 1)
            sleep(0.1)
            caput_assert(prefixdig + (":Dwn%d-SmNm" % ch), get_max_acq(prefix, prefixdig, 1))
            sleep(0.1)

    """
    Test to check if after a simulated pulse any data was acquired
    """
    def test_acq(self, prefixdig, board):
        for ch in range(0, 10):
            # disable decimation
            caput_assert(prefixdig + (":Dwn%d-DecEn" % ch), 0)
            # enable channel
            caput_assert(prefixdig + (":Dwn%d-En") % ch, 1)
            # enable transfer channel
            caput_assert(prefixdig + (":Dwn%d-EnTransf") % ch, 1)

        # test for the 3 formats
        for fmt in range(3):
            for ch in range(0, 10):
                # change DAQ format
                caput_assert(prefixdig + (":Dwn%d-Fmt") % ch, fmt)

            for state in ["RESET", "INIT", "ON"]:
                assert change_state(prefixdig, state)
            # run a pulse
            sim_bp_trig(board)

            ca.poll(1)
            for ch in range(0, 10):
                smnm = caget_assert(prefixdig + (":Dwn%d-AcqSm" % ch))
                assert smnm != 0

                # get read the value
                vals_imag = caget_assert(prefixdig + (":Dwn%d-Cmp0") % ch)

                vals_imag_avg = np.average(vals_imag)
                # at least one of 2 positions shouldn't be 0 (we expect that some
                # noise should be read)
                assert vals_imag_avg != 0

                assert vals_imag_avg < 2 and vals_imag_avg > -2

                if fmt != 2: # DC has only one component
                    vals_qang = caget_assert(prefixdig + (":Dwn%d-Cmp1") % ch)
                    vals_qang_avg = np.average(vals_qang)
                    assert vals_qang_avg != 0
                    assert vals_qang_avg < 1000 and vals_qang_avg > -1000
    """
    Check if all the channels have different values between each other
    """
    def test_different(self, prefix, prefixdig, board):
        nsamples = randint(1, get_max_acq(prefix, prefixdig, 1))
        for ch in range(0, 10):
            # disable decimation
            caput_assert(prefixdig + (":Dwn%d-DecEn" % ch), 0)
            # enable channel
            caput_assert(prefixdig + (":Dwn%d-En") % ch, 1)
            # enable transfer channel
            caput_assert(prefixdig + (":Dwn%d-EnTransf") % ch, 1)
            # set the number of samples
            caput_assert(prefixdig + (":Dwn%d-SmNm") % ch, nsamples)


        # test for the 3 formats
        for fmt in range(3):
            for ch in range(0, 10):
                # change DAQ format
                caput_assert(prefixdig + (":Dwn%d-Fmt") % ch, fmt)

            for state in ["RESET", "INIT", "ON"]:
                assert change_state(prefixdig, state)
            # run a pulse
            sim_bp_trig(board)

            sleep(2)
            for chA in range(0, 8):
                for chB in range(0, 8):
                    if chA != chB:
                        # get read the value
                        vals_imagA = caget_assert(prefixdig + (":Dwn%d-Cmp0") % chA)
                        vals_imagB = caget_assert(prefixdig + (":Dwn%d-Cmp0") % chB)

                        # values should be != 0
                        assert np.average(vals_imagA) != 0
                        assert np.average(vals_imagB) != 0

                        # values on different channels should be different
                        assert np.average(vals_imagA) != np.average(vals_imagB)

                        if fmt != 2: # DC has only one component
                            vals_qangA = caget_assert(prefixdig + (":Dwn%d-Cmp1") % chA)
                            vals_qangB = caget_assert(prefixdig + (":Dwn%d-Cmp1") % chB)
                            assert np.average(vals_qangA) != 0
                            assert np.average(vals_qangB) != 0

                            assert np.average(vals_qangA) != np.average(vals_qangB)


    """
    Test X Axis for all channels
    """
    def test_xax_ch(self, prefix, prefixdig, board):
        # test without decimation and setting SmNm at ON state
        for ch in range(0, 10):
            print("Channel ", ch)
            for state in ["RESET", "INIT", "ON"]:
                assert change_state(prefixdig, state)

            caput_assert(prefixdig + (":Dwn%d-DecF" % ch), 1)
            caput_assert(prefixdig + (":Dwn%d-DecEn" % ch), 0)
            smnm = randint(1, get_max_acq(prefix, prefixdig, 1))
            caput_assert(prefixdig + (":Dwn%d-SmNm" % ch), smnm)
            ca.poll(1)
            # run 2 pulses (to update values)
            sim_bp_trig(board)
            ca.poll(1)
            sim_bp_trig(board)
            ca.poll(1)

            smnm_rbv = caget_assert(prefixdig + (":Dwn%d-SmNm-RB" % ch))
            assert smnm == int(smnm_rbv)

            assert caget_assert(prefixdig + (":Dwn%d-DecEn-RB" % ch)) == 0
            assert caget_assert(prefixdig + (":Dwn%d-DecF-RB" % ch)) == 1

            f_samp = caget_assert(prefix + ":FreqSampling")
            near_iq_n = caget_assert(prefixdig + ":IQSmplNearIQN-RB")


            # check first position
            vals = caget_assert(prefixdig + (":Dwn%d-XAxis") % ch)
            assert vals is not None
            assert vals[0] == 0

            smnm_acq = caget_assert(prefixdig + (":Dwn%d-AcqSm" % ch))
            # check a random position
            pos = randint(1, smnm_acq)

            assert '%.6f' % vals[pos] == '%.6f' % (pos * (1/(f_samp * 1000)*near_iq_n))

            # check last position
            pos = int(smnm_acq) - 1
            print("Pos ", pos)
            assert '%.6f' % vals[pos] == '%.6f' % (pos * (1/(f_samp * 1000)*near_iq_n))
        # test for the 3 formats
        for fmt in range(3):
            for ch in range(0, 10):
                # change DAQ format
                caput_assert(prefixdig + (":Dwn%d-Fmt") % ch, fmt)



        # test with decimation
        for ch in range(0, 10):
            for state in ["RESET", "INIT"]:
                assert change_state(prefixdig, state)

            f_samp = caget_assert(prefix + ":FreqSampling")
            near_iq_n = caget_assert(prefixdig + ":IQSmplNearIQN-RB")

            assert check_readback(prefixdig + (":Dwn%d-DecF" % ch), 1)
            smnm = randint(1, get_max_acq(prefix, prefixdig, 1))
            max_dec = get_max_dec(prefix, prefixdig, smnm)
            dec_fac = randint(1, max_dec)

            caput_assert(prefixdig + (":Dwn%d-SmNm" % ch), smnm)
            sleep(0.1)
            smnm_rbv = caget_assert(prefixdig + (":Dwn%d-SmNm-RB" % ch))
            assert smnm == int(smnm_rbv)

            assert check_readback(prefixdig + (":Dwn%d-DecEn" % ch), 1)
            assert check_readback(prefixdig + (":Dwn%d-DecF" % ch), dec_fac)

            assert change_state(prefixdig, "ON")
            ca.poll(1)

            # run a pulse
            sim_bp_trig(board)

            ca.poll(1)
            # check first position
            vals = caget_assert(prefixdig + (":Dwn%d-XAxis") % ch)
            assert vals is not None
            assert vals[0] == 0

            smnm_acq = caget_assert(prefixdig + (":Dwn%d-AcqSm" % ch))
            # check a random position
            maxi = (smnm_acq//dec_fac)  - 1
            if maxi > 1 :
                pos = randint(1, maxi)
            else:
                pos = 1

            assert '%.6f' % vals[pos] == '%.6f' % ((pos*dec_fac) * (1/(f_samp * 1000)*near_iq_n))

            # check last position
            pos = int(maxi) - 1
            assert '%.6f' % vals[pos] == '%.6f' % ((pos*dec_fac) * (1/(f_samp * 1000)*near_iq_n))

    def test_normal_behavior(self, prefixdig, board):
        # check if the system keep working correctly
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        sim_bp_trig(board)
        sim_bp_trig(board)
        ca.poll(1)
        pulsecnt = caget_assert(prefixdig + ":PulseDoneCnt")
        assert pulsecnt  == 2
