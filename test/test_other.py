#!/usr/bin/env python3
from helper import read_reg, change_state, check_readback, sim_bp_trig, caget_assert, caput_assert

from random import randint
from time import sleep

class TestState:
    def test_states(self, prefixdig):
        for state in ["RESET", "OFF", "INIT", "ON", "RESET", "INIT"]:
            assert change_state(prefixdig, state)


def test_fsystem(prefix):
    # check if PV exists
    fsystem = caget_assert(prefix + ":FreqSystem")

    # check if it is not changeable
    caput_assert(prefix + ":FreqSystem", fsystem + 1)
    new_fsystem = caget_assert(prefix + ":FreqSystem")
    assert new_fsystem == fsystem

def test_ctrl_input_sel(prefix, prefixdig, board):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)
        for ch in range(8):
            check_readback(prefixdig + ":ChGrpAI-CtrlInSel", ch)

        assert change_state(prefixdig, "ON")
        ch = randint(0, 7)
        caput_assert(prefixdig + ":ChGrpAI-CtrlInSel", ch)

        sim_bp_trig(board)

        sleep(1)
        assert caget_assert(prefixdig + ":ChGrpAI-CtrlInSel-RB") == ch
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # return to 0
        check_readback(prefixdig + ":ChGrpAI-CtrlInSel", 0)
