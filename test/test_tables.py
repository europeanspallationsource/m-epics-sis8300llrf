#!/usr/bin/env python3
from random import random, randint

from helper import FF_REG, SP_REG, read_reg, float_to_fixed, get_min_max_mn, read_mem, fixed_to_float
from helper import caput_assert, caget_assert
import numpy as np

NELM = 16

# Normal mode

# SP -  I
# SP -  Q
# FF -  I
# FF -  Q

# Special mode

# SP - SM - I
# SP - SM - Q
# FF - SM - I
# FF - SM - Q

def set_table(prefixdig, ctrl, spec, qi, qmn, size = 16):
    """Set values for one table (the same value)
    ctrl = SP / FF
    spec = "" / S
    qi = I / Q
    size = number of elements
    qmn = (m, n, signed)
    """
    (min, max) = get_min_max_mn(*qmn)

    val = round(random()*(max-min)+min, qmn[1])
    while (val > max or val < min):
        val = round(random()*(max-min)+min, qmn[1])

    vals = []
    vals.extend([val]*size)

    pv = prefixdig + ":FwTbl" + spec + "-" +  ctrl

    caput_assert(pv + "-" + qi, vals)

    # write table to the memory
    caput_assert(pv + "-TblToFW", 1)

    # update readback
    caput_assert(pv + "-Get" + qi + ".PROC", 1)

    return val

class TestTablesNormal:
    """Class for test tables on Normal mode"""
    def test_tables_sp_qi(self, board, prefixdig):
        """Test tables SP Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(prefixdig, "SP", "", "Q", qmn)
        i_val = set_table(prefixdig, "SP", "", "I", qmn)

        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))

        # get a random position to check
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":FwTbl-SP-GetI")
        rbv_q = caget_assert(prefixdig + ":FwTbl-SP-GetQ")
        assert fixed_to_float(float_to_fixed(i_val, *qmn),*qmn) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn),*qmn) == rbv_q[pos]

    def test_tables_ff_qi(self, board, prefixdig):
        """Test tables FF Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(prefixdig, "FF", "", "Q", qmn)
        i_val = set_table(prefixdig, "FF", "", "I", qmn)

        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))

        # get a random position to check
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":FwTbl-FF-GetI")
        rbv_q = caget_assert(prefixdig + ":FwTbl-FF-GetQ")
        assert fixed_to_float(float_to_fixed(i_val, *qmn),*qmn) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn),*qmn) == rbv_q[pos]

class TestTablesSpecial():
    """Class for test tables on Normal mode on normal mode"""
    def test_tables_sp_mag_ang(self, board, prefixdig):
        """Test tables SP Mag and Ang"""
        qmn_ang = (3, 13, 1)
        qmn_mag_sp = (0, 16, 0)

        ang_val = set_table(prefixdig, "SP", "S", "Ang", qmn_ang)
        mag_val = set_table(prefixdig, "SP", "S", "Mag", qmn_mag_sp)

        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        ang_val_qmn = str(float_to_fixed(ang_val, *qmn_ang))
        mag_val_qmn = str(float_to_fixed(mag_val, *qmn_mag_sp))

        # get a random position to check
        pos = randint(0, 15)
        # ang pos = pos*2
        # mag pos = pos*2 + 1

        assert ang_val_qmn == mem_values[pos*2]
        assert mag_val_qmn == mem_values[pos*2+1]
        # check the readback pv
        pos = randint(1, 15)
        rbv_mag = caget_assert(prefixdig + ":FwTblS-SP-GetMag")
        rbv_ang = caget_assert(prefixdig + ":FwTblS-SP-GetAng")
        assert fixed_to_float(float_to_fixed(ang_val, *qmn_ang),*qmn_ang) == rbv_ang[pos]
        assert fixed_to_float(float_to_fixed(mag_val, *qmn_mag_sp),*qmn_mag_sp) == rbv_mag[pos]

    def test_tables_ff_mag_ang(self, board, prefixdig):
        """Test tables FF Mag and Ang"""
        qmn_ang = (3, 13, 1)
        qmn_mag_ff = (1, 15, 1)

        ang_val = set_table(prefixdig, "FF", "S", "Ang", qmn_ang)
        mag_val = set_table(prefixdig, "FF", "S", "Mag", qmn_mag_ff)

        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        ang_val_qmn = str(float_to_fixed(ang_val, *qmn_ang))
        mag_val_qmn = str(float_to_fixed(mag_val, *qmn_mag_ff))

        # get a random position to check
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert ang_val_qmn == mem_values[pos*2]
        assert mag_val_qmn == mem_values[pos*2+1]

        # check the readback pv
        pos = randint(1, 15)
        rbv_mag = caget_assert(prefixdig + ":FwTblS-FF-GetMag")
        rbv_ang = caget_assert(prefixdig + ":FwTblS-FF-GetAng")
        assert fixed_to_float(float_to_fixed(ang_val, *qmn_ang),*qmn_ang) == rbv_ang[pos]
        assert fixed_to_float(float_to_fixed(mag_val, *qmn_mag_ff),*qmn_mag_ff) == rbv_mag[pos]

    def test_tables_sp_qi(self, board, prefixdig):
        """Test tables SP Q and I """
        qmn_qi = (1, 15, 1)
        q_val = set_table(prefixdig, "SP", "S", "Q", qmn_qi)
        i_val = set_table(prefixdig, "SP", "S", "I", qmn_qi)

        mem_pos = int(read_reg(board, SP_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn_qi))
        i_val_qmn = str(float_to_fixed(i_val, *qmn_qi))

        # get a random position to check
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":FwTblS-SP-GetI")
        rbv_q = caget_assert(prefixdig + ":FwTblS-SP-GetQ")
        assert fixed_to_float(float_to_fixed(i_val, *qmn_qi),*qmn_qi) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn_qi),*qmn_qi) == rbv_q[pos]

    def test_tables_ff_qi(self, board, prefixdig):
        """Test tables FF Q and I """
        qmn_qi = (1, 15, 1)
        q_val = set_table(prefixdig, "FF", "S", "Q", qmn_qi)
        i_val = set_table(prefixdig, "FF", "S", "I", qmn_qi)

        mem_pos = int(read_reg(board, FF_REG), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2)
        q_val_qmn = str(float_to_fixed(q_val, *qmn_qi))
        i_val_qmn = str(float_to_fixed(i_val, *qmn_qi))

        # get a random position to check
        pos = randint(0, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2]
        assert i_val_qmn == mem_values[pos*2+1]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":FwTblS-FF-GetI")
        rbv_q = caget_assert(prefixdig + ":FwTblS-FF-GetQ")
        assert fixed_to_float(float_to_fixed(i_val, *qmn_qi),*qmn_qi) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn_qi),*qmn_qi) == rbv_q[pos]

