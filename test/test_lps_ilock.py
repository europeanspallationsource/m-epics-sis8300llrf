from time import sleep

from helper import caget_assert
from helper import change_state, write_reg, sim_bp_trig

class TestLPSIlock:
    def test_simulated(self, prefixdig, board):
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        write_reg(board, "0xF05", "0x1")
        write_reg(board, "0xF06", "0x1")

        sim_bp_trig(board)

        sleep(1)
        assert caget_assert(prefixdig + ":LPSIlckStat") == 1

        write_reg(board, "0xF05", "0x7")
        write_reg(board, "0xF06", "0x0")

        sim_bp_trig(board)
        sleep(1)
        sim_bp_trig(board)

        sleep(1)
        assert caget_assert(prefixdig + ":LPSIlckStat") == 0
