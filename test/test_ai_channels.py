#!/usr/bin/env python3
import pytest
from random import randint
from time import sleep

from helper import change_state, sim_bp_trig
from helper import caget_assert, caput_assert


class TestAIChannels:
    def test_en_all(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # enable all channels
        for c in range(0,10):
            caput_assert(prefixdig + (":AI%d-En") % c, 1)
            sleep(0.1)
            res = caget_assert(prefixdig + (":AI%d-En-RB") % c)
            assert res == 1

            res = caget_assert(prefixdig + (":AI%d-En-RB.STAT") % c)
            sleep(0.1)
            assert res == 0

            caput_assert(prefixdig + (":AI%d-EnTransf") % c, 1)

    @pytest.mark.parametrize("ch", list(range(0,10)))
    def test_acq(self, prefixdig, board, ch):
        """
        Test to check if after a simulated pulse any data was acquired
        """
        # enable channel
        caput_assert(prefixdig + (":AI%d-En") % ch, 1)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)
        # run a pulse
        sim_bp_trig(board)


        smnm = caget_assert(prefixdig + ":ChGrpAI-SmNm-RB")

        # check if the number of acquired elements is equal to smnm
        ch_size = caget_assert(prefixdig + (":AI%d.NORD") % ch)
        assert smnm == ch_size

        # get a random position
        pos = randint(0, smnm-1)
        pos2nd = randint(0, smnm-1)

        vals = caget_assert(prefixdig + (":AI%d") % ch)

        # get read the value
        vals = caget_assert(prefixdig + (":AI%d") % ch)

        # at least one of 2 positions should be 0 (we expect that some
        # noise should be read)
        assert (vals[pos] != 0) or (vals[pos2nd] != 0)

    def test_dis_01(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # try to disable AI0 and AI1
        for c in range(0,2):
            caput_assert(prefixdig + (":AI%d-En") % c, 0)
            sleep(0.1)
            res = caget_assert(prefixdig + (":AI%d-En-RB") % c)
            assert res == 1


    def test_dis_29(self, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # disable AI2-AI9 channels
        for c in range(2,10):
            caput_assert(prefixdig + (":AI%d-En") % c, 0)
            sleep(0.1)
            res = caget_assert(prefixdig + (":AI%d-En-RB") % c)
            assert res == 0

            res = caget_assert(prefixdig + (":AI%d-En-RB.STAT") % c)
            assert res == 0
