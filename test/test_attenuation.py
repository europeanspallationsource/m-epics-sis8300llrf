from random import random
from time import sleep

from helper import caput_assert
from helper import SIS8300DRV_RTM_ATT_VM, change_state, float_to_fixed, read_reg


class TestAttenuation:
    def set_rand_att(self, board, prefixdig, ch):
        m = 5
        n = 1

        # generate a random value
        max_value = 2**m - 2**-n
        att_val = random()*max_value

        # set value
        caput_assert(prefixdig + ":AI" + str(ch) + "-Att", str(att_val))
        valqmn = float_to_fixed(att_val, m, n, 0)

        # read from register
        reg = "0xF8" + str(2 + (ch//4))
        sleep(2)
        res = int(read_reg(board, reg), 16)

        # get the desired part
        rshift = ch % 4 * 8
        res = (res >> rshift) & 0x000000FF

        return valqmn == res

    def set_rand_att_vmout(self, board, prefixrtm):
        m = 4
        n = 2
        # generate a random value
        max_value = 2**m - 2**-n
        att_val = random()*max_value

        # set value
        caput_assert(prefixrtm + ":VMOUT-Att", str(att_val))
        valqmn = float_to_fixed(att_val, m, n, 0)

        # read from register
        reg = "0xF8" + str(2 + (8//4))
        sleep(2)
        res = int(read_reg(board, reg), 16)

        # get the desired part
        rshift = 8 % 4 * 8
        res = (res >> rshift) & 0x000000FF

        return valqmn == res


    def test_init(self, board, prefixdig, prefixrtm):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixdig, ch) == True

        assert self.set_rand_att_vmout(board, prefixrtm)  == True

    def test_on(self, board, prefixdig, prefixrtm):
        assert change_state(prefixdig, "ON")

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixdig, ch) == True

        assert self.set_rand_att_vmout(board, prefixrtm)  == True

    def test_after_reset(self, board, prefixdig, prefixrtm):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixdig, ch) == True

        assert self.set_rand_att_vmout(board, prefixrtm)  == True
