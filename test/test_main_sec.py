from time import sleep

from helper import caget_assert, caput_assert
from helper import change_state, check_readback

class TestMainSec:
    def test_main(self, prefixdig, prefixrtm):
        caput_assert(prefixdig + ":Main.DISP", 0)
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":Main", 1)

        assert check_readback(prefixrtm + ":VM-En", 1)
        assert check_readback(prefixrtm + ":VM-En", 0)

    def test_sec(self, prefixdig, prefixrtm):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":Main", 0)

        assert not check_readback(prefixrtm + ":VM-En", 1)

        assert caget_assert(prefixrtm + ":VM-En.STAT") == 2 and caget_assert(prefixrtm + ":VM-En.SEVR") == 3

    """
    Changing from Main -> SEC -> Main should wokr only on INIT state
    """
    def test_on(self, prefixdig):
        assert check_readback(prefixdig + ":Main", 0)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        assert not check_readback(prefixdig + ":Main", 1)

        assert caget_assert(prefixdig + ":Main.STAT") == 2 and caget_assert(prefixdig + ":Main.SEVR") == 3

        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":Main", 1)

