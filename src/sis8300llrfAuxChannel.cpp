/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAuxChannel.cpp
 * @brief File defining the LLRF Aux channel class that is used by
 * #sis8300llrfAuxChannel and #sis8300llrfInternalChannel as a base
 * class
 * @author gabriel.fedel@ess.eu
 * @date 7.4.2020
 * 
 * TODO: add long description
 */
#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAuxChannel.h"

std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_ENABLE          = "AUXEnable";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_NSAMPLES_ACQ    = "AUXNSamplesAcq";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_SAMPLES_COUNT   = "AUXSamplesCount";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_DEC_ENABLE      = "AUXDecEnable";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_DEC_FACTOR      = "AUXDecFactor";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_XAX             = "AUXXAxis";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_ENABLE_TRANSF   = "AUXEnableTransf";
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_IMAG            = "AUXIMag"; 
std::string sis8300llrfAuxChannel::
                    PV_REASON_AUX_QANG            = "AUXQAng";


/**
 * @brief Aux channel constructor.
 */
sis8300llrfAuxChannel::sis8300llrfAuxChannel(epicsFloat64 FSampling, 
        epicsInt32 nearIqN,
        int downOrIntern) ://0 : Down-Sampled / 1: Internal
        sis8300llrfChannel(
            SIS8300LLRFDRV_AUX_PARAM_NUM, 
            SIS8300LLRFDRV_AUX_PARAM_INT_FIRST) {

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfAuxChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfAuxChannel::onEnterReset, this));

    _SamplesAcquired = 0;
    _FSampling = FSampling;
    _NearIqN = nearIqN;
    _XAxis = NULL;
    _EnableTransf = 0;
    _XAxisChanged = 0;
    _DownOrIntern = downOrIntern;

    _LimitSamplesDec = (MAXINTERVAL*_FSampling*1000)/_NearIqN;
}

/**
 * @brief AI channel destructor.
 *
 */
sis8300llrfAuxChannel::~sis8300llrfAuxChannel() {
    if (_XAxis != NULL)
        delete [] _XAxis;
}


/**
 * @see   sis8300llrfChannel:onEnterReset
 * @brief Aditionaly to parent, also check statuses
 */
ndsStatus sis8300llrfAuxChannel::onEnterReset() {
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}

/**
 * @brief Read all the aux data from memory an push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the aux data, convert to float and push
 * the the PV.
 *
 * */
ndsStatus sis8300llrfAuxChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
   
    int iter;
    epicsInt32 en, dec_en, dec_fact;
    double param;

     readParameter(aux_param_enable, &param);
     en = (epicsInt32) param;

    NDS_DBG("Enable transference %d, enable on fw %d", _EnableTransf, en);
    if (!_EnableTransf || !en) { //The data transfer is disabled
        if (_XAxis != NULL)
            delete [] _XAxis;
        _XAxis = NULL;
        _SamplesAcquired = 0;
        doCallbacksInt32(_SamplesAcquired, _interruptIdAUXNSamplesAcq);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXXAxis);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXIMag);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        return ndsSuccess;
    }


    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }

    
    if (_SamplesAcquired == 0) {
        return ndsSuccess;
    }

    if (_XAxisChanged) {
        _XAxisChanged = 0;
        /* Update XAxis corresponding to acquired data
         * Note FSampling is considered constant after IOC initialisation.
         * There is no way for user to change the FSampling value after IOC initialisation. */
        if (_XAxis != NULL)
            delete [] _XAxis;
        _XAxis = new epicsFloat64[_SamplesAcquired];
        if (!_XAxis) {
            NDS_ERR("Cannot allocate memory for down-sampled channel %d X-Axis data.", _Channel);
            SIS8300NDS_MSGERR("Cannot allocate memory for down-sampled channel X-Axis");
            error();
            return ndsError;
        }
        NDS_DBG("Auxiliar (down-sampled or Internal) Channel %d allocated buffer for %u X-Axis points.", _Channel, _SamplesAcquired);
        // check decimation enable and decimation factor
        // calling readParameter, because is needed to read value used on
        // last acquistion (could be different of value available locally if
        // the value was change before the last acquisition)
        readParameter(aux_param_dec_enable, &param);
        dec_en = (epicsInt32) param;
        if (dec_en){
            readParameter(aux_param_dec_factor, &param);
            dec_fact = (epicsInt32) param;
        }
        else
            dec_fact = 1;

        /* Go ahead and populate XAxis at this point as we have all required info */
        for (iter = 0; iter < _SamplesAcquired; iter++) {
                _XAxis[iter] = (epicsFloat64) (iter * dec_fact) * 1/(_FSampling * 1000)*_NearIqN;
        }
        // update PV
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXXAxis);
    }

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfAuxChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_aux_param(
            _DownOrIntern,
            _DeviceUser,  _Channel, 
            (sis8300llrfdrv_aux_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfAuxChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_aux_param(
                _DownOrIntern,
                _DeviceUser, _Channel,
                (sis8300llrfdrv_aux_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfAuxChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_ENABLE,
           &sis8300llrfAuxChannel::setAUXEnable,
           &sis8300llrfAuxChannel::getAUXEnable,
           &_interruptIds[aux_param_enable]
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_NSAMPLES_ACQ,
           NULL,
           &sis8300llrfAuxChannel::getAUXNSamplesAcq,
           &_interruptIdAUXNSamplesAcq
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_SAMPLES_COUNT,
           &sis8300llrfAuxChannel::setAUXSamplesCount,
           &sis8300llrfAuxChannel::getAUXSamplesCount,
           &_interruptIds[aux_param_samples_cnt]
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_DEC_ENABLE,
           &sis8300llrfAuxChannel::setAUXDecEnable,
           &sis8300llrfAuxChannel::getAUXDecEnable,
           &_interruptIds[aux_param_dec_enable]
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_DEC_FACTOR,
           &sis8300llrfAuxChannel::setAUXDecFactor,
           &sis8300llrfAuxChannel::getAUXDecFactor,
           &_interruptIds[aux_param_dec_factor]
     );

    NDS_PV_REGISTER_FLOAT64ARRAY(
           sis8300llrfAuxChannel::PV_REASON_AUX_XAX,
           NULL,
           &sis8300llrfAuxChannel::getAUXXAxis,
           &_interruptIdAUXXAxis
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfAuxChannel::PV_REASON_AUX_ENABLE_TRANSF,
           &sis8300llrfAuxChannel::setAUXEnableTransf,
           &sis8300llrfAuxChannel::getAUXEnableTransf,
           &_interruptIdAUXEnableTransf
     );


     NDS_PV_REGISTER_FLOAT64ARRAY(
           sis8300llrfAuxChannel::PV_REASON_AUX_IMAG,
           NULL,
           &sis8300llrfAuxChannel::getFloat64Array,
           &_interruptIdAUXIMag
     );

     NDS_PV_REGISTER_FLOAT64ARRAY(
           sis8300llrfAuxChannel::PV_REASON_AUX_QANG,
           NULL,
           &sis8300llrfAuxChannel::getFloat64Array,
           &_interruptIdAUXQAng
     );

    _Channel = getChannelNumber() - SIS8300DRV_NUM_AI_CHANNELS*_DownOrIntern;

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/**
 * @brief Set the aux channel to enable/disable
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Number of samples to ignore
 * 
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfAuxChannel::setAUXEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[aux_param_enable] = (int) value;

    _ParamChanges[aux_param_enable] = 1;
    return commitParameters();
}
/**
 * @brief Check if the aux channel is enable/disable
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disable
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfAuxChannel::getAUXEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_enable];

    return ndsSuccess;
}

/**
 * @brief Get the number of acquired samples
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Will hold the number of samples acquired on 
 *                          success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfAuxChannel::getAUXNSamplesAcq(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    int status;
    unsigned nsamples;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }
    status = sis8300llrfdrv_get_acquired_nsamples_aux(_DeviceUser, 
            _DownOrIntern, _Channel, &nsamples);

    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_acquired_nsamples", status);
    _SamplesAcquired = (epicsInt32) nsamples;

    *value = _SamplesAcquired;

    return ndsSuccess;
}

/**
 * @brief Set the number of samples to acquire
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Number of samples to acquire
 * 
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfAuxChannel::setAUXSamplesCount(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    epicsInt32 dec_fac;
 
    NDS_DBG("Samples count. Decimation factor %d", int(_ParamVals[aux_param_dec_factor]));
    dec_fac = _ParamVals[aux_param_dec_factor];

    //this is used only by PIERRILC channel
    if (dec_fac < 1)
        dec_fac = 1;
    //check if the value is in the interval limit
    if (value > _LimitSamplesDec/dec_fac)
        _ParamVals[aux_param_samples_cnt] = (int) _LimitSamplesDec/dec_fac;
    else
        _ParamVals[aux_param_samples_cnt] = (int) value;

    _ParamChanges[aux_param_samples_cnt] = 1;
    return commitParameters();
}
/**
 * @brief Get the number of samples to acquire
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Number of samples to acquire
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfAuxChannel::getAUXSamplesCount(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_samples_cnt];

    return ndsSuccess;
}

/**
 * @brief Set the decimation to enable/disable
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Enable/Disable
 * 
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfAuxChannel::setAUXDecEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[aux_param_dec_enable] = (int) value;

    _ParamChanges[aux_param_dec_enable] = 1;
    return commitParameters();
}
/**
 * @brief Check if the decimation is enable/disable
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       will hold decimation enable/disable value
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfAuxChannel::getAUXDecEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_dec_enable];

    return ndsSuccess;
}

/**
 * @brief Set the decimation Factor 
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       decimation factor
 * 
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfAuxChannel::setAUXDecFactor(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    //check if the value is in the interval limit
    if (value > _LimitSamplesDec/_ParamVals[aux_param_samples_cnt])
        _ParamVals[aux_param_dec_factor] = (int) _LimitSamplesDec/_ParamVals[aux_param_samples_cnt];
    else
        _ParamVals[aux_param_dec_factor] = value;

    if (_ParamVals[aux_param_dec_factor] < 1)
        _ParamVals[aux_param_dec_factor] = 1;

    _ParamChanges[aux_param_dec_factor] = 1;
    return commitParameters();
}
/**
 * @brief Get the decimation Factor
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       will hold decimation factor value
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfAuxChannel::getAUXDecFactor(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_dec_factor];

    return ndsSuccess;
}

/**
 * @brief Get the X-Axis for this channel
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] outArray    Waveform with X axis       
 * @param [out] nIn         Number of elements read
 * 
 * @return always  ndsSuccess
 *
 * Return the X axis array and its size.
 * */

ndsStatus sis8300llrfAuxChannel::getAUXXAxis(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn) {
   NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    outArray = _XAxis;
    *nIn = _SamplesAcquired;

    return ndsSuccess;
}

/**
 * @brief Enable the data transfer
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Enable/Disable
 * 
 * @return always  ndsSuccess
 *
 * Set a private variable tha enable or disable the data transfer
 * from memory. This doesn't change anything on firmware, but IOC
 * will log load de acquired data.
 */
ndsStatus sis8300llrfAuxChannel::setAUXEnableTransf(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _EnableTransf = value;

    doCallbacksInt32(_EnableTransf, _interruptIdAUXEnableTransf);

    //Should define XAxis again if the transf was enabled
    if (_EnableTransf == 1)
        _XAxisChanged = 1;
    return ndsSuccess;
}

/**
 * @brief Read if data transfer is Enable 
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disable
 * 
 * @return ndsSuccess       Always success
 * 
 * This will return if the data transfer is enable or not
 */
ndsStatus sis8300llrfAuxChannel::getAUXEnableTransf(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);

    *value = (epicsInt32) _EnableTransf;

    return ndsSuccess;
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
ndsStatus sis8300llrfAuxChannel::checkStatuses() {
    epicsInt32 valInt32;

    if (getAUXNSamplesAcq(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }
    //If the number of samples is different should update the XAxis
    if (_SamplesAcquired != valInt32)
        _XAxisChanged = 1;

    _SamplesAcquired = valInt32;
    doCallbacksInt32(_SamplesAcquired, _interruptIdAUXNSamplesAcq);

    return ndsSuccess;
}

/**
* @brief Reimplement commitParameters to check if decimation changed
* if it's happened should update XAxis on next pulse;
*/
ndsStatus sis8300llrfAuxChannel::commitParameters() {
    NDS_TRC("%s", __func__);
    sis8300llrfChannelGroup *cg = 
            dynamic_cast<sis8300llrfChannelGroup*> (getChannelGroup());

    if ( !_Initialized ||
         cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }
    //if decimation or number of samples to be aquired changed, update XAxis on next pulse
    if (_ParamChanges[aux_param_dec_enable] || _ParamChanges[aux_param_dec_factor] || _ParamChanges[aux_param_samples_cnt] || _ParamChanges[aux_param_enable])
        _XAxisChanged = 1;
    
    return writeToHardware();
}
