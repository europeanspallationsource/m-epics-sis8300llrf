/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfControlTableChannel.cpp
 * @brief Implementation of the LLRF control table (FF and SP) channel 
 *        class.
 * @author urojec
 * @date 26.5.2014
 * 
 * sis8300llrfControlTableChannel class has a defined type - each CH can 
 * be a Feed Forward (FF) or Set Point (SP) table. Set point tables are 
 * used during ramp up phase and FF tables are used during pulse on. The
 * channel contains two tables - I and Q which are than joined together 
 * to form one table that is written down to hardware. If one of the 
 * tables is shorter than the other, the shorter one is filled up with 
 * the last value to fit the larger table. This is ok, because the 
 * controller will hold the last value anyway.
 * Each CH represents one pulse type, the number of the CH is the same
 * as pulse type number or id.
 *
 * When in special mode the channel can contain Angle and Magnitude
 * Table instead of I and Q, these tables reuse the buffers for I and Q
 * Angle is written to Q and Magnitude to I. Two  extra setters and getters 
 * for angle and magnitude table are provided, since the conversion of 
 * double to device's fixed point representation and back is different. 
 * Due to shared buffers, it is extreamly importaint that both I and Q 
 * or magnitude and angle table  are both written down when a special 
 * operating mode needs to be used.
 * 
 * For now there is only one pulse type.
 */

#include <string.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControlTableChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfControlTableChannel::PV_REASON_Q_TABLE = 
                                                "QTable";
std::string sis8300llrfControlTableChannel::PV_REASON_I_TABLE  = 
                                                "ITable";
std::string sis8300llrfControlTableChannel::PV_REASON_RAW_TABLE = 
                                                "RawTable";
std::string sis8300llrfControlTableChannel::PV_REASON_TABLE_MODE = 
                                                "TableMode";
std::string sis8300llrfControlTableChannel::PV_REASON_WRITE_TABLE = 
                                                "WriteTable";
std::string sis8300llrfControlTableChannel::PV_REASON_ANGLE_TABLE = 
                                                  "AngleTable";
std::string sis8300llrfControlTableChannel::PV_REASON_MAG_TABLE = 
                                                        "MagTable";


/**
 * @brief sis8300llrfControlTableChannel Constructor
 */
sis8300llrfControlTableChannel::sis8300llrfControlTableChannel() :
        sis8300llrfChannel(0,0) {

    _RawTablePrel = NULL;
    _RawTableFinal = NULL;

    _TableSet = 0;
    _WriteTable = 0;

    _MaxSamplesCount = -1;
    _SamplesCount = -1;
    _TableMode = (sis8300llrfdrv_table_mode) table_mode_normal;
}

/**
 * @brief sis8300llrfControlTableChannel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfControlTableChannel::~sis8300llrfControlTableChannel() {
    freeBuffers();
}

epicsFloat64 sis8300llrfControlTableChannel::_FFSPConvFact = 
         (epicsFloat64) (1 << sis8300llrfdrv_Qmn_IQ_sample.frac_bits_n);

epicsFloat64 sis8300llrfControlTableChannel::_FFSPSampleMin =
         (epicsFloat64) (-pow(2, sis8300llrfdrv_Qmn_IQ_sample.int_bits_m - 1));

epicsFloat64 sis8300llrfControlTableChannel::_FFSPSampleMax =
         (epicsFloat64) (pow(2, sis8300llrfdrv_Qmn_IQ_sample.int_bits_m - 1) - 
                         pow(2, -(int)sis8300llrfdrv_Qmn_IQ_sample.frac_bits_n));

epicsFloat64 sis8300llrfControlTableChannel::_MagFFConvFact =
         (epicsFloat64) (1 << sis8300llrfdrv_Qmn_ff_mag.frac_bits_n);

epicsFloat64 sis8300llrfControlTableChannel::_MagFFSampleMin =
         (epicsFloat64) (-pow(2, sis8300llrfdrv_Qmn_ff_mag.int_bits_m - 1));

epicsFloat64 sis8300llrfControlTableChannel::_MagFFSampleMax =
         (epicsFloat64) (pow(2, sis8300llrfdrv_Qmn_ff_mag.int_bits_m - 1) -
                         pow(2, -(int)sis8300llrfdrv_Qmn_ff_mag.frac_bits_n));

epicsFloat64 sis8300llrfControlTableChannel::_MagSPConvFact =
         (epicsFloat64) (1 << sis8300llrfdrv_Qmn_sp_mag.frac_bits_n);

epicsFloat64 sis8300llrfControlTableChannel::_MagSPSampleMin =
         (epicsFloat64) (-pow(2, sis8300llrfdrv_Qmn_sp_mag.int_bits_m - 1));

epicsFloat64 sis8300llrfControlTableChannel::_MagSPSampleMax =
         (epicsFloat64) (pow(2, sis8300llrfdrv_Qmn_sp_mag.int_bits_m - 1) -
                         pow(2, -(int)sis8300llrfdrv_Qmn_sp_mag.frac_bits_n));

epicsFloat64 sis8300llrfControlTableChannel::_AngleConvFact =
         (epicsFloat64) (1 << sis8300llrfdrv_Qmn_angle.frac_bits_n);

epicsFloat64 sis8300llrfControlTableChannel::_AngleSampleMin =
         (epicsFloat64) (-pow(2, sis8300llrfdrv_Qmn_angle.int_bits_m - 1));

epicsFloat64 sis8300llrfControlTableChannel::_AngleSampleMax =
         (epicsFloat64) (pow(2, sis8300llrfdrv_Qmn_angle.int_bits_m - 1) -
                         pow(2, -(int)sis8300llrfdrv_Qmn_angle.frac_bits_n));


/**
 * @brief Initialize the control table channel
 *
 * @return ndsSuccess   everything ok, or if already initialized
 * @return ndsError     unrecognized table type selected, table not 
 *                      allocated
 *
 * Allocate and intitalize the buffers for the tables. If any of
 * them fail, put channel to ERROR state.
 *
 * If an unknown control table type is selected, prevent going on
 * with NDS_CRT status
 */
ndsStatus sis8300llrfControlTableChannel::initialize() {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    int status;
    unsigned regVal;
    sis8300llrfdrv_table_mode ffMode;

    if (_Initialized) {
        /* Inform of reinitialization, because the tables will be 
         * reset */
        NDS_WRN("Reinitializing %s: this will RESET ALL THE TABLES!", 
            _ChanStringIdentifier);
    }

    freeBuffers();

    /* Create string identifier for error messages */
    sprintf(_ChanStringIdentifier, "PT: %i, cg: %s", getChannelNumber(), 
        getChannelGroup()->getName().c_str());

    _CtrlTableType =
            dynamic_cast<sis8300llrfControlTableChannelGroup*>
                (getChannelGroup())->getCtrlTableType();
                
    switch (_CtrlTableType) {
        case ctrl_table_sp:
            break;
        case ctrl_table_ff:
            status = sis8300llrfdrv_get_table_mode(
                            _DeviceUser,_CtrlTableType, &ffMode);
            SIS8300NDS_STATUS_ASSERT(
                "sis8300llrddrv_get_table_mode", status);
            doCallbacksInt32(
                (epicsInt32) ffMode, _interruptIdTableMode);
            break;
        default:
            NDS_CRT("Invalid control table type for %s.", 
                _ChanStringIdentifier);
            return ndsError;
    }

    _SamplesCount = 1;
    _QNelm = 1;
    _INelm = 1;

    /* Get max samples and allocate the buffers for the tables
     * Max samples is the largest they can be */
    status = sis8300llrfdrv_get_ctrl_table_max_nelm(
                _DeviceUser, _CtrlTableType, &regVal);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_ctrl_table_max_nsamples", status);
    _MaxSamplesCount = (epicsInt32) regVal;

    _RawTablePrel = (epicsInt32 *) 
                calloc(sizeof(epicsInt32), (size_t) _MaxSamplesCount);
    if (!_RawTablePrel) {
        NDS_ERR("Could not allocate buffer for preliminary raw table %s", 
            _ChanStringIdentifier);
        error();
        return ndsError;
    }

    _RawTableFinal = (epicsInt32 *) 
                calloc(sizeof(epicsInt32), (size_t) _MaxSamplesCount);
    if (!_RawTableFinal) {
        NDS_ERR("Could not allocate buffer for final raw table %s", 
            _ChanStringIdentifier);
        error();
        return ndsError;
    }

    NDS_DBG("Allocated buffer for tables, %s. Buff nsamples %#10x", 
        _ChanStringIdentifier, _MaxSamplesCount);

    _SamplesCount = 1;
    _WriteTable = 1;
    
    writeToHardware();

    /* Set the initialized flag,
     * buffers are allocated so tables can now be written down*/
    _Initialized = 1;

    return ndsSuccess;
}

/**
 * @see @sis8300llrfChannel::writeToHardware
 */
ndsStatus sis8300llrfControlTableChannel::writeToHardware(){
    NDS_TRC("%s %i", __func__, getChannelNumber());

    int status, iterDest;
    unsigned nsamples;
    sis8300llrfdrv_table_mode TableModeRead;
 
    sis8300llrfControlTableChannelGroup *cg = 
        dynamic_cast<sis8300llrfControlTableChannelGroup*> 
            (getChannelGroup());

    if (_WriteTable) {
        _WriteTable = 0;

        /* we need to accecss device memory in 512 bit blocks - firmware imposed limitation 
         * Fill the rest with zeros. */
        NDS_DBG("Round _SamplesCount from %d", _SamplesCount);
        nsamples = ROUNDUP_TWOHEX(_SamplesCount);
        NDS_DBG("_SamplesCount rounded to %u", nsamples);
        for (iterDest = _SamplesCount; 
                iterDest < (int) nsamples; iterDest++) {
            _RawTableFinal[iterDest] = 0;
        }

        NDS_DBG("Writing new raw table for %s", _ChanStringIdentifier);
        status = sis8300llrfdrv_set_ctrl_table_raw(
                    _DeviceUser, _CtrlTableType, 
                    (unsigned) getChannelNumber(), nsamples, _RawTableFinal);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_ctrl_table_raw", status);

        /* This is now done on demand rather than with a callback 
         * doCallbacksInt32Array(_RawTable, (size_t) nsamples, _interruptIdRawTable);
         */

        /* If this is the currently active table, we need to set the update
         * reason because the controller needs to reload the table */
        if (cg->getActiveTable() == getChannelNumber()) {

            if (_CtrlTableType == ctrl_table_sp) {
                cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_SP);
            }
            else if (_CtrlTableType == ctrl_table_ff) {
                cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_FF);
            }

            NDS_DBG("%s: Wrote new table for active pulse type.", 
                _ChanStringIdentifier);
        }
    }

    /* If this is the currently active table, update reasons need to be 
     * fixed for the controller to take new parameters into account */
    if (cg->getActiveTable() == getChannelNumber()) {

        if (_SamplesCountChange) {
            /* if samples count changed, this was flagged in 
             * @see #fillTable */
            _SamplesCountChange = 0;
            cg->setSamplesCount(NULL, _SamplesCount);
            NDS_DBG("%s: Wrote new samples count for active pulse"
                "type.", _ChanStringIdentifier);
        }

        if (_TableModeChange) {
            _TableModeChange = 0;

            status = sis8300llrfdrv_set_table_mode(
                        _DeviceUser, _CtrlTableType,
                        (sis8300llrfdrv_table_mode) _TableMode);
            SIS8300NDS_STATUS_ASSERT(
                "sis8300llrfdrv_set_table_mode", status);
            NDS_DBG("Table mode set to %i, for chan %i", 
                _TableMode, getChannelNumber());

            status = sis8300llrfdrv_get_table_mode(
                            _DeviceUser,_CtrlTableType, &TableModeRead);
            SIS8300NDS_STATUS_ASSERT(
                "sis8300llrddrv_get_table_mode", status);

            if (TableModeRead != _TableMode) {
                NDS_ERR("Written value is different from read value for Table Mode. Written value %d Read Value %d", _TableMode, TableModeRead);
                return ndsError;
            }

            cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);

            doCallbacksInt32(_TableMode, _interruptIdTableMode);
        }
    }

    return sis8300llrfChannel::writeToHardware();
}

/**
 * @see #sis8300llrfChannel::markAllParamtersAsChanged
 */
ndsStatus sis8300llrfControlTableChannel::markAllParametersChanged() {

    _SamplesCountChange = 1;

    _TableModeChange = 1;

    return ndsSuccess;
}

/**
 * @brief Was the table set. This flag is used to determine if the 
 *        specific pulse type is set up from @see #sis8300llrfDevice
 *
 * @return 1 if yes, 0 if no
 */
int sis8300llrfControlTableChannel::tableSet() {
    return _TableSet;
}

/**
 * @brief Free control table buffers
 */
void sis8300llrfControlTableChannel::freeBuffers() {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    if (_RawTablePrel) {
        NDS_DBG("Freeing preliminary raw table data buffer for %s", 
            _ChanStringIdentifier);
        free(_RawTablePrel);
        _RawTablePrel = NULL;
    }

    if (_RawTableFinal) {
        NDS_DBG("Freeing final table data buffer for %s", 
            _ChanStringIdentifier);
        free(_RawTableFinal);
        _RawTableFinal = NULL;
    }

    _TableSet = 0;
}

/* ============== GETTERS, SETTERS and HANDLER REGISTRATION ========= */

/**
 * @brief Setter for number of elements in the control table. Setting 
 *        is not allowed manually.
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value      element number
 *
 * @retval ndsError   Always
 * 
 * This setting is not allowed. The samples count is always set when the 
 * table is written. The channel group 
 * ( @see #sis8300llrfControlTableChannelGroup ) holds the sample count 
 * for the currently active table.
 */
ndsStatus sis8300llrfControlTableChannel::setSamplesCount(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_ERR("Samples count for Control table channel is not directly "
        "settable");
    return ndsError;
}

/**
 * @brief Get the current element count in the table
 *
 * @param [in]  pasynUser  Asyn user
 * @param [out] value      Will hold samples count on success
 *
 * @return ndsSucess Value retrieved
 * @return ndsError  At IOC init
 */
ndsStatus sis8300llrfControlTableChannel::getSamplesCount(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _SamplesCount;

    return ndsSuccess;
}

/**
 *  * @brief Set table
 *  *
 *  * @param [in] pasynUser Asyn user struct.
 *  * @param [in] inArray      table
 *  * @param [in] nelem         Number of samples in the table
 *  *
 *  * @retval ndsSuccess set ok
 *  * @retval ndsError wrong nelm or value out of range.
 */
ndsStatus sis8300llrfControlTableChannel::setTable(
             epicsFloat64 *value, size_t nelem,
             unsigned int *tableNelm, unsigned int memOffset,
             epicsFloat64 convFact, epicsFloat64 sampleMin, 
             epicsFloat64 sampleMax, unsigned int isSigned) {
    NDS_TRC("%s %i", __func__, getChannelNumber());
    
    epicsInt16 *rawTableI16 = (epicsInt16*) _RawTablePrel;
    epicsUInt16 *rawTableUI16 = (epicsUInt16*) _RawTablePrel;
    unsigned int iterSrc, iterDest;

    /* Copy current final table to preliminary table.*/
    memcpy(_RawTablePrel, _RawTableFinal, sizeof(epicsInt32)*_MaxSamplesCount);

    if (!_Initialized) {
        return ndsError;
    }

    /* read only the _SamplesCount of elements, because at ioc init the 
     * nelm is set to maximum size of the table */
    if ((epicsInt32) nelem > _MaxSamplesCount) {
        NDS_ERR("%s: Q table is too large to use, nelm=%i, max=%i.",
            _ChanStringIdentifier, (int)nelem, (int)_MaxSamplesCount);
        return ndsError;
    }

    /* Convert to raw value and put in preliminary raw table */
    if (isSigned) {
        for (iterSrc = 0, iterDest = memOffset;
             iterSrc < nelem; iterSrc++, iterDest += 2) {
            /* Check range of element values */
            if ( value[iterSrc] < sampleMin ||
                value[iterSrc] > sampleMax ) {
               NDS_ERR("%s: Element %d equals %f in table, which is out of range."
                       "Valid range is between %f to %f.\n", _ChanStringIdentifier,
                       iterSrc, value[iterSrc], sampleMin, sampleMax);
               return ndsError;
           }
           rawTableI16[iterDest] = (epicsInt16) (value[iterSrc] * convFact);
       }
    } else {
        for (iterSrc = 0, iterDest = memOffset;
            iterSrc < nelem; iterSrc++, iterDest += 2) {
            /* Check range of element values */
            if ( value[iterSrc] < sampleMin ||
                 value[iterSrc] > sampleMax ) {
                NDS_ERR("%s: Element %d equals %f in table, which is out of range."
                        "Valid range is between %f to %f.\n", _ChanStringIdentifier,
                        iterSrc, value[iterSrc], sampleMin, sampleMax);
            return ndsError;
            }
            rawTableUI16[iterDest] = (epicsUInt16) (value[iterSrc] * convFact);
        }
    }

    /* Set control variables and final table equal to preliminary table*/
    _TableSet  = 1;
    *tableNelm = (unsigned int) nelem;
    memcpy(_RawTableFinal, _RawTablePrel, sizeof(epicsInt32)*_MaxSamplesCount);

    fillTable();

    return ndsSuccess;


}

/**
 * @brief Set the Q table
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] inArray      table
 * @param [in] nelem         Number of samples in the table
 *
 * @retval ndsSuccess set ok
 * @retval ndsError wrong nelm or value out of range.
 */
ndsStatus sis8300llrfControlTableChannel::setQTable(
                asynUser* pasynUser, epicsFloat64 *value, size_t nelem) {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    return setTable(value, nelem, &_QNelm, SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET,
                    _FFSPConvFact, _FFSPSampleMin, _FFSPSampleMax, 
                    sis8300llrfdrv_Qmn_IQ_sample.is_signed );
}

/**
 * @brief Read the current Q table for this channel (=pulse type) from 
 *        hw
 *
 * @param [in]  pasynUser Asyn user struct.
 * @param [out] value     Pointer to the buffer where the table should 
 *                        be written
 * @param [in]  nelm      Size of the provided buffer
 * @param [out] nIn       Number of actual elements in the returned 
 *                        table
 *
 * @retval ndsSuccess     Data retrieved successfully
 * @retval ndsError       Provided buffer size is too small
 *
 * Not really meant to be used for pulse to pulse readouts, but more 
 * during setup. It will read the raw table from hardware, extract the Q 
 * part of the table and convert from fixed point format on the device 
 * to doubles.
 */
ndsStatus sis8300llrfControlTableChannel::getQTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
    NDS_TRC("%s %i", __func__, getChannelNumber());
    //This temporary pointer is used to load the data from memory
    epicsFloat64 *tmp = new epicsFloat64[nelem];

    /* I and Q tables are interleaved. We fetch them as on table with
     * 32 bit ellements from the device and than read it as two 16 bit
     * tables */
    epicsInt16 *pInt16Buffer = (epicsInt16 *) tmp;
    int iterSrc, iterDest;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        delete tmp;
        return ndsError;
    }

    if (nelem < (size_t) _SamplesCount) {
        NDS_ERR(" %s: Provided buffer is too small to fetch the table,"
            "table size=%i, nelm=%i.", _ChanStringIdentifier, 
            (int) _SamplesCount, (int) nelem);
        delete tmp;
        return ndsError;
    }

    if (getRawTable(pasynUser, (epicsInt32 *)tmp, nelem, nIn) 
            != ndsSuccess) {
        delete tmp;
        return ndsError;
    }

    for(iterDest = 0, iterSrc = SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET; 
            iterDest < _SamplesCount; iterDest++, iterSrc += 2) {
        value[iterDest] = 
            ( (epicsFloat64) pInt16Buffer[iterSrc] ) / _FFSPConvFact;
    }
    
    delete tmp;
    return ndsSuccess;
}

/**
 * @brief Set the I table
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] inArray      table
 * @param [in] nelm         Number of samples in the table
 *
 * @retval ndsSuccess set ok
 * @retval ndsError wrong nelm
 */
ndsStatus sis8300llrfControlTableChannel::setITable(
                asynUser* pasynUser, epicsFloat64 *value, size_t nelem) {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    return setTable(value, nelem, &_INelm, SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET,
                    _FFSPConvFact, _FFSPSampleMin, _FFSPSampleMax,
                    sis8300llrfdrv_Qmn_IQ_sample.is_signed);
}

/**
 * @brief Get the current I table for this channel (=pulse type)
 *
 * @param [in]  pasynUser Asyn user struct.
 * @param [out] value     Pointer to the buffer where the table should 
 *                        be written
 * @param [in]  nelm      Size of the provided buffer
 * @param [out] nIn       Number of actual elements in the returned 
 *                        table
 *
 * @retval ndsSuccess     Data retrieved successfully
 * @retval ndsError       Provided buffer size is too small
 *
 * Not really meant to be used for pulse to pulse readouts, but more 
 * during setup. It will read the raw table from hardware, extract the I 
 * part of the table and convert from fixed point format on the device 
 * to doubles.
 */
ndsStatus sis8300llrfControlTableChannel::getITable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
    NDS_TRC("%s %i", __func__, getChannelNumber());
    //This temporary pointer is used to load the data from memory
    epicsFloat64 *tmp = new epicsFloat64[nelem];

    /* I and Q tables are interleaved. We fetch them as on table with
     * 32 bit ellements from the device and than read it as two 16 bit
     * tables */
    epicsInt16 *pInt16Buffer = (epicsInt16 *) tmp;
    int iterSrc, iterDest;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        delete tmp;
        return ndsError;
    }
    
    if ( nelem < (size_t) _SamplesCount) {
        NDS_ERR(" %s: Provided buffer is too small to fetch the table, "
            "table size=%i, nelm=%i.", _ChanStringIdentifier, 
            (int) _SamplesCount, (int) nelem);
        delete tmp;
        return ndsError;
    }
    
    if (getRawTable(pasynUser, (epicsInt32 *)tmp, nelem, nIn) 
            != ndsSuccess) {
        delete tmp;
        return ndsError;
    }

    for(iterDest = 0, iterSrc = SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET; 
            iterDest < _SamplesCount; iterDest++, iterSrc += 2) {
        value[iterDest] = 
            ( (epicsFloat64) pInt16Buffer[iterSrc] )/ _FFSPConvFact;
    }
    delete tmp;

    return ndsSuccess;
}

/** 
 * @brief Read the raw table for this channel (=pulse type) 
 *        from the controller and return it without converting.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Pointer to the buffer where the table should 
 *                          be written
 * @param [in]  nelem       Maximum size of the table
 * @param [out] nIn         Actual number of elements in the table
 * 
 * This will only read the table in the same format as it is on the 
 * device. If separated tables for I and Q part with converted values 
 * are required, they can be obtained with #getITable and #getQTable 
 * respectively.
 */
ndsStatus sis8300llrfControlTableChannel::getRawTable(
                asynUser *pasynUser, epicsInt32 *value, 
                size_t nelem, size_t *nIn) {
    int status;


    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION ||
        _device->getCurrentState() == nds::DEVICE_STATE_OFF) {
        return ndsError;
    }

    if (nelem < (size_t) _SamplesCount) {
        NDS_ERR(" %s: Provided buffer is too small to fetch the table, "
            "table size=%i, nelm=%i.", _ChanStringIdentifier, 
            (int) _SamplesCount, (int) nelem);
        return ndsError;
    }

    status = sis8300llrfdrv_get_ctrl_table_raw(
            _DeviceUser, _CtrlTableType, (unsigned) getChannelNumber(), 
            ROUNDUP_TWOHEX(_SamplesCount), value);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_ctrl_table_raw", status);

    *nIn = (size_t) _SamplesCount;

    return ndsSuccess;
}

/**
 * @brief Internal function to join both I and Q table in to hw ready 
 *        format. Extend length to make table of the same size if 
 *        necessary.
 *
 *
 * Check if the elemnum in the tables is the same, and make them the 
 * same if not. Since the controller will hold the last value until the 
 * end of the ACTIVE_NO_PULSE in case of SP and ACTIVE_PULSE in case of 
 * FF, we just fill the table with the last value 
 */
inline void sis8300llrfControlTableChannel::fillTable() {
    epicsInt16 *rawTable16 = (epicsInt16 *) _RawTableFinal;

    int iterLast, iterDest, iterElem, iterElemMax;

    if (_QNelm < _INelm) {
        iterLast = (_QNelm - 1) * 2 + SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET;
        iterElem = _QNelm;
        iterElemMax = _INelm;

        NDS_INF("Q table too short (%i), filling up to I table "
            "size (%i)", _QNelm, _INelm);
    } else if (_INelm < _QNelm) {
        iterLast = (_INelm - 1) * 2 + SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET;
        iterElem = _INelm;
        iterElemMax = _QNelm;

        NDS_INF("I table table too short (%i), filling up to Q table "
            "size (%i)", _INelm, _QNelm);
    } else {
        NDS_INF("Q and I table if the same size");
        iterElem = iterElemMax = _INelm;
    }

    for (iterDest = iterLast + 2; 
            iterElem < iterElemMax; iterElem++, iterDest += 2) {
        rawTable16[iterDest] = rawTable16[iterLast];
    }

    _SamplesCount = (epicsInt32) iterElemMax;
    _SamplesCountChange = 1;

    doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);

    return;
}

/**
 * @brief set write table
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        not used, will set write table to 1, when called.
 * 
 * @return ndsError     If the channel is not initalized
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfControlTableChannel::setWriteTable(
                asynUser *pasynUser, epicsInt32 value) {
    if (!_Initialized) {
        return ndsError;
    }
    _WriteTable = 1;
    
    return commitParameters();
}

/**
 * @brief Set table mode to either hold last or circular
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 0 for hold last and 1 for circular
 * 
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfControlTableChannel::setTableMode(
                asynUser *pasynUser, epicsInt32 value) {
    _TableMode = (sis8300llrfdrv_table_mode) value;

    _TableModeChange = 1;
    return commitParameters();
}

/**
 * @brief Get the table mode from the controller
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Will be 0 if mode is hold last and 1 if it 
 *                           is circular
 * 
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfControlTableChannel::getTableMode(
                asynUser *pasynUser, epicsInt32 *value) {
    *value = _TableMode;

    return ndsSuccess;
}



/* ======================================================= */
/* === SETTERS AND GETTERS FOR SPECIAL OPERATION MODES === */
/* ======================================================= */

/**
 * @brief Set the angle table used in special operating modes
 *
 * @param [in] pasynUser    Asyn user context struct.
 * @param [in] value        Table to write
 * @param [in] nelm         Number of samples in the table
 *
 * @return ndsSuccess   Set successfull
 * @return ndsError     Number of elements in the table is bigger 
 *                      than the maximum allowed on the controller
 * 
 * the table will be converted from double to device fixed point format 
 * and joined with the magnitude table to a raw table that can be 
 * written to the controller.
 * Since there is no special operating mode that would simoultaneously 
 * use MA and IQ and since these are located on the same section of the 
 * device memory the software usses Q table buffer for magnitude table 
 * and I table buffer for angle table. The difference is in the 
 * conversion of the data for MA or IQ samples.
 * 
 * It is importaint that the MA and IQ pair are always written down 
 * together!
 */
ndsStatus sis8300llrfControlTableChannel::setAngleTable(
                asynUser* pasynUser, epicsFloat64 *value, size_t nelem) {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    return setTable(value, nelem, &_QNelm, SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET,
                    _AngleConvFact, _AngleSampleMin, _AngleSampleMax,
                    sis8300llrfdrv_Qmn_angle.is_signed);
}

/** 
 * @brief Read the angle part of the control table for this CH 
 *        (=pulse type) from the controller and return it without 
 *        converting.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Pointer to the buffer where the table should 
 *                          be written
 * @param [in]  nelem       Maximum size of the table
 * @param [out] nIn         Actual number of elements in the table
 * 
 * The table will be read out from hw and than converted to double. 
 */
ndsStatus sis8300llrfControlTableChannel::getAngleTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
    NDS_TRC("%s %i", __func__, getChannelNumber());
    //This temporary pointer is used to load the data from memory
    epicsFloat64 *tmp = new epicsFloat64[nelem];

    epicsInt16 *pInt16Buffer = (epicsInt16 *) tmp;
    epicsFloat64 convFact = (epicsFloat64) 
                    (1 << sis8300llrfdrv_Qmn_angle.frac_bits_n);
    int iterSrc, iterDest;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        delete tmp;
        return ndsError;
    }

    if (getRawTable(pasynUser, (epicsInt32 *)tmp, nelem, nIn) 
            != ndsSuccess) {
        delete tmp;
        return ndsError;
    }

    for(iterDest = 0, iterSrc = SIS8300LLRFDRV_RAW_SAMPLE_Q_OFFSET; 
            iterDest < _SamplesCount; iterDest++, iterSrc += 2) {
        value[iterDest] = 
            ( (epicsFloat64) pInt16Buffer[iterSrc] )/ convFact;
    }

    delete tmp;
    return ndsSuccess;
}


/**
 * @brief Set the angle table used in special operating modes
 *
 * @param [in] pasynUser    Asyn user context struct.
 * @param [in] value        Table to write
 * @param [in] nelm         Number of samples in the table
 *
 * @return ndsSuccess   Set successfull
 * @return ndsError     Number of elements in the table is bigger 
 *                      than the maximum allowed on the controller
 * 
 * the table will be converted from double to device fixed point format 
 * and joined with the angle table to a raw table that can be written
 * to the controller.
 * Since there is no special operating mode that would simoultaneously 
 * use MA and IQ and since these are located on the same section of the 
 * device memory the software usses Q table buffer for magnitude table 
 * and I table buffer for angle table. The difference is in the 
 * conversion of the data for MA or IQ samples.
 * 
 * It is importaint that the MA and IQ pair are always written down 
 * together!
 */
ndsStatus sis8300llrfControlTableChannel::setMagnitudeTable(
                asynUser* pasynUser, epicsFloat64 *value, size_t nelem) {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    if (_CtrlTableType == ctrl_table_ff) {
        return setTable(value, nelem, &_INelm, SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET,
                        _MagFFConvFact, _MagFFSampleMin, _MagFFSampleMax,
                        sis8300llrfdrv_Qmn_ff_mag.is_signed);
    } else if (_CtrlTableType == ctrl_table_sp) {
        return setTable(value, nelem, &_INelm, SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET,
                        _MagSPConvFact, _MagSPSampleMin, _MagSPSampleMax,
                        sis8300llrfdrv_Qmn_sp_mag.is_signed);
    } else {
        return ndsError;
    }
}

/** 
 * @brief Read the magnitude part of the control table for this CH 
 *        (= pulse type) from the controller and return it without 
 *        converting.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Pointer to the buffer where the table should 
 *                          be written
 * @param [in]  nelem       Maximum size of the table
 * @param [out] nIn         Actual number of elements in the table
 * 
 * The table will be read out from hw and than converted to double. 
 */
ndsStatus sis8300llrfControlTableChannel::getMagnitudeTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
    NDS_TRC("%s %i", __func__, getChannelNumber());
    //This temporary pointer is used to load the data from memory
    epicsFloat64 *tmp = new epicsFloat64[nelem];

    epicsInt16 *pInt16Buffer = (epicsInt16 *) tmp;
    epicsUInt16 *pUInt16Buffer = (epicsUInt16 *) tmp;

    epicsFloat64 convFact;
    epicsInt32 isSigned;
    
    int iterSrc, iterDest;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        delete tmp;
        return ndsError;
    }

    if (getRawTable(pasynUser, (epicsInt32 *)tmp, nelem, nIn) 
            != ndsSuccess) {
        delete tmp;
        return ndsError;
    }

    if (_CtrlTableType == ctrl_table_ff) {
        convFact = _MagFFConvFact;
        isSigned = sis8300llrfdrv_Qmn_ff_mag.is_signed;
    } else if (_CtrlTableType == ctrl_table_sp){
        convFact = _MagSPConvFact;
        isSigned = sis8300llrfdrv_Qmn_sp_mag.is_signed;
    } else {
        delete tmp;
        return ndsError;
    }

    if (isSigned) {
        for(iterDest = 0, iterSrc = SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET; 
                iterDest < _SamplesCount; iterDest++, iterSrc += 2) {
            value[iterDest] = 
                ( (epicsFloat64) pInt16Buffer[iterSrc] ) / convFact;
        }
    } else {
        for(iterDest = 0, iterSrc = SIS8300LLRFDRV_RAW_SAMPLE_I_OFFSET; 
                iterDest < _SamplesCount; iterDest++, iterSrc += 2) {
            value[iterDest] = 
                ( (epicsFloat64) pUInt16Buffer[iterSrc] ) / convFact;
        }
    }

    delete tmp;
    return ndsSuccess;
}




/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfControlTableChannel::registerHandlers(
        nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfControlTableChannel::PV_REASON_Q_TABLE,
            &sis8300llrfControlTableChannel::setQTable,
            &sis8300llrfControlTableChannel::getQTable,
            &_interruptIdQTable);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfControlTableChannel::PV_REASON_I_TABLE,
            &sis8300llrfControlTableChannel::setITable,
            &sis8300llrfControlTableChannel::getITable,
            &_interruptIdITable);

    NDS_PV_REGISTER_INT32ARRAY(
            sis8300llrfControlTableChannel::PV_REASON_RAW_TABLE,
            &sis8300llrfControlTableChannel::setInt32Array,
            &sis8300llrfControlTableChannel::getRawTable,
            &_interruptIdRawTable);

    NDS_PV_REGISTER_INT32(
            sis8300llrfControlTableChannel::PV_REASON_TABLE_MODE,
            &sis8300llrfControlTableChannel::setTableMode,
            &sis8300llrfControlTableChannel::getTableMode,
            &_interruptIdTableMode);

    NDS_PV_REGISTER_INT32(
            sis8300llrfControlTableChannel::PV_REASON_WRITE_TABLE,
            &sis8300llrfControlTableChannel::setWriteTable,
            &sis8300llrfControlTableChannel::getInt32,
            &_interruptIdWriteTable);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfControlTableChannel::PV_REASON_ANGLE_TABLE,
            &sis8300llrfControlTableChannel::setAngleTable,
            &sis8300llrfControlTableChannel::getAngleTable,
            &_interruptIdAngleTable);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfControlTableChannel::PV_REASON_MAG_TABLE,
            &sis8300llrfControlTableChannel::setMagnitudeTable,
            &sis8300llrfControlTableChannel::getMagnitudeTable,
            &_interruptIdMagTable);

    return ADIOChannel::registerHandlers(pvContainers);
}
