/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIErrorChannel.h
 * @brief Header file defining the LLRF PIError channel class that handles
 * internal values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 9.4.2020
 */

#ifndef _sis8300llrfPIErrorChannel_h
#define _sis8300llrfPIErrorChannel_h

#include "sis8300llrfAuxChannel.h"
#include "sis8300AIChannel.h"

/**
 * @brief sis8300 LLRF specific sis8300llrfAuxChannel class that supports 
 *  PIError channel
 */
class sis8300llrfPIErrorChannel: public sis8300llrfAuxChannel {
public:
    sis8300llrfPIErrorChannel(epicsFloat64 FSampling, epicsInt32 nearIqN);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setDAQFormat(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getDAQFormat(
                        asynUser *pasynUser, epicsInt32 *value);


    virtual ndsStatus setRMSReset(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setRMSSMNMIgnore(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getRMSSMNMIgnore(
                        asynUser *pasynUser, epicsInt32 *value);
protected:
    int          _RMSNsamplesIgnore; /**< Holds the number of samples 
                                       * that are ignored at the end of 
                                       * the wf when callculating the 
                                       * #_RMSCurrent */
    epicsFloat64 _RMSCurrentI;        /**< Current RMS value for I*/
    epicsFloat64 _RMSCurrentQ;        /**< Current RMS value for Q*/
    epicsFloat64 _RMSAverageI;        /**< Average RMS value for the last 
                                       * #_RMSPulseCnt pulses for I*/
    epicsFloat64 _RMSAverageQ;        /**< Average RMS value for the last 
                                       * #_RMSPulseCnt pulses for Q*/
    epicsFloat64 _RMSMaxI;            /**< Maximum RMS value for I in the last
                                       * #_RMSPulseCnt pulses */
    epicsFloat64 _RMSMaxQ;            /**< Maximum RMS value for Q in the last
                                       * #_RMSPulseCnt pulses */
    epicsInt32   _RMSPulseCnt;       /**< Number of RMS values from 
                                       * which the _RMSAverage is 
                                       * callculated */
    epicsInt32   _RMSReset;          /**< Reset the #_RMSAverage and 
                                       * #_RMSMax and set #_RMSPulseCnt 
                                       * to 0 */

    /*For asyn reasons*/
    static std::string PV_REASON_PIERROR_DAQ_FMT;
    static std::string PV_REASON_RMS_CURRENT_I;
    static std::string PV_REASON_RMS_CURRENT_Q;
    static std::string PV_REASON_RMS_SMNM_IGNORE;
    static std::string PV_REASON_RMS_MAX_I;
    static std::string PV_REASON_RMS_MAX_Q;
    static std::string PV_REASON_RMS_AVERAGE_I;
    static std::string PV_REASON_RMS_AVERAGE_Q;
    static std::string PV_REASON_RMS_PULSECNT;
    static std::string PV_REASON_RMS_RESET;

    int _interruptIdRMSCurrentI;
    int _interruptIdRMSCurrentQ;
    int _interruptIdRMSSMNMIgnore;  
    int _interruptIdRMSAverageI;
    int _interruptIdRMSAverageQ;
    int _interruptIdRMSPulseCount;
    int _interruptIdRMSMaxI;
    int _interruptIdRMSMaxQ;
    int _interruptIdRMSReset;

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onEnterProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfPIErrorChannel_h */
