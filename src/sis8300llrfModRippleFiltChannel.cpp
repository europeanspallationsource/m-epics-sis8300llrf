/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @file sis8300llrfModRippleFilChannel.cpp
 * @brief Implementation of sis8300llrf Modulator ripple filter channel in NDS
 * @author urojec
 * @date 23.1.2015
 * 
 * This class holds all the settings related to the modulator ripple filter that
 * are available on the device. It is more or less a collection of setters and getters,
 * since this unit has no readouts that have to be made on pulse-to-pulse basis.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfModRippleFiltChannel.h"

std::string sis8300llrfModRippleFiltChannel::
                    PV_REASON_CONST_S    = "ModRippleFilConstS";
std::string sis8300llrfModRippleFiltChannel::
                    PV_REASON_CONST_C    = "ModRippleFilConstC";
std::string sis8300llrfModRippleFiltChannel::
                    PV_REASON_CONST_A    = "ModRippleFilConstA";
std::string sis8300llrfModRippleFiltChannel::
                    PV_REASON_Q_EN       = "ModRippleFilQEn";
std::string sis8300llrfModRippleFiltChannel::
                    PV_REASON_I_EN       = "ModRippleFilIEn";


/**
 * @brief sis8300llrfModRippleFiltChannel constructor
 */
sis8300llrfModRippleFiltChannel::sis8300llrfModRippleFiltChannel() :
        sis8300llrfChannel(
            SIS8300LLRFDRV_MOD_RIPPLE_PARAM_NUM, 
            SIS8300LLRFDRV_MOD_RIPPLE_PARAM_INT_FIRST) {

    sprintf(_ChanStringIdentifier, "Mod Ripple filter");
}


/**
 * @brief sis8300llrfModRippleFiltChannel destructor
 */
sis8300llrfModRippleFiltChannel::~sis8300llrfModRippleFiltChannel() {}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfModRippleFiltChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_mod_ripple_param(_DeviceUser,
            (sis8300llrfdrv_mod_ripple_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfModRippleFiltChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_mod_ripple_param(_DeviceUser,
                (sis8300llrfdrv_mod_ripple_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfModRippleFiltChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfModRippleFiltChannel::PV_REASON_CONST_S,
            &sis8300llrfModRippleFiltChannel::setConstantS,
            &sis8300llrfModRippleFiltChannel::getConstantS,
            &_interruptIds[mod_ripple_fil_const_s]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfModRippleFiltChannel::PV_REASON_CONST_C,
            &sis8300llrfModRippleFiltChannel::setConstantC,
            &sis8300llrfModRippleFiltChannel::getConstantC,
            &_interruptIds[mod_ripple_fil_const_c]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfModRippleFiltChannel::PV_REASON_CONST_A,
            &sis8300llrfModRippleFiltChannel::setConstantA,
            &sis8300llrfModRippleFiltChannel::getConstantA,
            &_interruptIds[mod_ripple_fil_const_a]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfModRippleFiltChannel::PV_REASON_Q_EN,
           &sis8300llrfModRippleFiltChannel::setQEnable,
           &sis8300llrfModRippleFiltChannel::getQEnable,
           &_interruptIds[mod_ripple_fil_Q_en]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfModRippleFiltChannel::PV_REASON_I_EN,
           &sis8300llrfModRippleFiltChannel::setIEnable,
           &sis8300llrfModRippleFiltChannel::getIEnable,
           &_interruptIds[mod_ripple_fil_I_en]);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ======= RELATED TO MODULATOR RIPPLE FILTER ======== */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set modulator ripple filter constant S
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the S constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfModRippleFiltChannel::setConstantS(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[mod_ripple_fil_const_s] = (double) value;

    _ParamChanges[mod_ripple_fil_const_s] = 1;
    return commitParameters();
}
/**
 * @brief Get modulator filter constant S
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the S constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfModRippleFiltChannel::getConstantS(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[mod_ripple_fil_const_s];

    return ndsSuccess;
}
/**
 * @brief Set modulator ripple filter constant C
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the C constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfModRippleFiltChannel::setConstantC(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[mod_ripple_fil_const_c] = (double) value;

    _ParamChanges[mod_ripple_fil_const_c] = 1;
    return commitParameters();
}
/**
 * @brief Get modulator filter constant C
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the C constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfModRippleFiltChannel::getConstantC(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[mod_ripple_fil_const_c];

    return ndsSuccess;
}
/**
 * @brief Set modulator ripple filter constant A
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the A constant to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfModRippleFiltChannel::setConstantA(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[mod_ripple_fil_const_a] = (double) value;

    _ParamChanges[mod_ripple_fil_const_a] = 1;
    return commitParameters();
}
/**
 * @brief Get modulator filter constant A
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the A constant value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfModRippleFiltChannel::getConstantA(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[mod_ripple_fil_const_a];

    return ndsSuccess;
}
/**
 * @brief Enable modulator ripple filter for Q
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfModRippleFiltChannel::setQEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[mod_ripple_fil_Q_en] = (double) value;

    _ParamChanges[mod_ripple_fil_Q_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if modulator ripple flter is enabled for Q
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfModRippleFiltChannel::getQEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[mod_ripple_fil_Q_en];

    return ndsSuccess;
}
/**
 * @brief Enable modulator ripple filter for I
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfModRippleFiltChannel::setIEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[mod_ripple_fil_I_en] = (double) value;

    _ParamChanges[mod_ripple_fil_I_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if modulator ripple flter is enabled for I
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfModRippleFiltChannel::getIEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[mod_ripple_fil_I_en];

    return ndsSuccess;
}
