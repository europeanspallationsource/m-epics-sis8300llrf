/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannel.cpp
 * @brief Implementation of LLRF specific channel in NDS.
 * @author urojec
 * @date 5.6.2014
 * 
 * This channel is only used for cavity (AI0) and reference (AI1) 
 * signals and adds the functionality of reading out the current
 * Magnitude and Angle (MA) point. IQ point is also callculated
 * from the MA point by software. 
 * 
 * The class also makes sure that the two channels never get disabled.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAIChannel.h"

/**
 * @brief llrf channel constructor.
 */
sis8300llrfAIChannel::sis8300llrfAIChannel() : sis8300AIChannel() {
    _isEnabled = 1;
}


sis8300llrfAIChannel::sis8300llrfAIChannel(epicsFloat64 FSampling) : sis8300AIChannel(FSampling) {
    _isEnabled = 1;
}

/**
 * @brief LLRF AI channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfAIChannel::~sis8300llrfAIChannel() {}

/**
 * @brief Override parent, because we are not allowed to disable 
 *        reference and cavity channel. The PI loop does not 
 *        even start with AI0 disabled.
 */
ndsStatus sis8300llrfAIChannel::setEnabled(
    asynUser *pasynUser, epicsInt32 value) {

    NDS_TRC("%s",__func__);
    if (value == 0) {
        NDS_ERR("AI Channel 0 (CAV) and REF (1) are always enabled");
        return ndsError;
    }

    return sis8300AIChannel::setEnabled(pasynUser, 1);
}
