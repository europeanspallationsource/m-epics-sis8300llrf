/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfControlTableChannel.h
 * @brief Header file defining the LLRF control table (FF and SP) channel 
 * class.
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.5.2014
 */

#ifndef _sis8300llrfControlTableChannel_h
#define _sis8300llrfControlTableChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports 
 * ControlTable setup
 *
 * Each control Table acts as an analog output channel. Channel number 
 * corresponds to pulse type.
 */
class sis8300llrfControlTableChannel: public sis8300llrfChannel {
public:
    sis8300llrfControlTableChannel();
    virtual ~sis8300llrfControlTableChannel();

    virtual ndsStatus initialize();

    virtual int tableSet();
    virtual ndsStatus markAllParametersChanged();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setSamplesCount(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getSamplesCount(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setTableMode(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getTableMode(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setWriteTable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTable(
                        epicsFloat64 *value, size_t nelem,
                        unsigned int *tableNelm, unsigned int memOffset,
                        epicsFloat64 convFact, epicsFloat64 sampleMin,
                        epicsFloat64 sampleMax, unsigned int isSigned);
    virtual ndsStatus setQTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getQTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus setITable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getITable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus getRawTable(
                        asynUser *pasynUser, epicsInt32 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus setAngleTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getAngleTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus setMagnitudeTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getMagnitudeTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);

protected:
    sis8300llrfdrv_ctrl_table _CtrlTableType; /**< Table type, either SP 
                                                *  or FF */
    epicsInt32 *_RawTablePrel;      /**< Holds the preliminary table in hw format */
    epicsInt32 *_RawTableFinal;     /**< Holds the final table in hw format */
    epicsInt32 _TableMode;        /**< Can be hold last (normal) or 
                                      *  circular (special operation) */

    epicsInt32 _SamplesCount;       /**< Table size for this pulse type */
    epicsInt32 _MaxSamplesCount;    /**< Maximum allowed number of table 
                                      *  elements. Equals to 
                                      *  _SamplesCount from Channel 
                                      * group */

    int        _WriteTable;         /**< Parameter to trigger write
                                      * of tables */
    int        _SamplesCountChange; /** < Parameter Tracking Samples 
                                      * count changes */
    int        _TableModeChange;  /** < Parameter tracking Table 
                                      * mode changes */
    int        _TableSet;           /**< Set to 1 the first time table 
                                      * is written - so that we know 
                                      * table is set up */

    unsigned int  _QNelm;                     /**< Number of elements in the Angle 
                                      * table */
    unsigned int  _INelm;                     /**< Number of elements in the angle 
                                      * table */

    static epicsFloat64 _FFSPConvFact;  /**< For converting samples from/to 
                                          * device's fixed point to/from 
                                          * double */
    
    static epicsFloat64 _FFSPSampleMin;  /**< For checking minimum limit
                                           *  on samples */
    
    static epicsFloat64 _FFSPSampleMax;  /**< For checking maximum limits
                                           *  on samples */

    static epicsFloat64 _MagFFConvFact;  /**< For converting samples from/to 
                                          * device's fixed point to/from 
                                          * double */

    static epicsFloat64 _MagFFSampleMin;  /**< For checking minimum limit
                                           *  on samples */

    static epicsFloat64 _MagFFSampleMax;  /**< For checking maximum limits
                                           *  on samples */

    static epicsFloat64 _MagSPConvFact;  /**< For converting samples from/to 
                                          * device's fixed point to/from 
                                          *                                           * double */

    static epicsFloat64 _MagSPSampleMin;  /**< For checking minimum limit
                                           *  on samples */

    static epicsFloat64 _MagSPSampleMax;  /**< For checking maximum limits
                                           *  on samples */

    static epicsFloat64 _AngleConvFact;  /**< For converting samples from/to 
                                          * device's fixed point to/from 
                                          * double */

    static epicsFloat64 _AngleSampleMin;  /**< For checking minimum limit
                                           *  on samples */

    static epicsFloat64 _AngleSampleMax;  /**< For checking maximum limits
                                           *  on samples */


    
    /* write to hardware */
    virtual ndsStatus writeToHardware();

    /* for asynReasons */
    static std::string PV_REASON_Q_TABLE;
    static std::string PV_REASON_I_TABLE;
    static std::string PV_REASON_RAW_TABLE;
    static std::string PV_REASON_TABLE_MODE;
    static std::string PV_REASON_WRITE_TABLE;
    static std::string PV_REASON_ANGLE_TABLE;
    static std::string PV_REASON_MAG_TABLE;

    int _interruptIdQTable;
    int _interruptIdITable;
    int _interruptIdRawTable;
    int _interruptIdTableMode;
    int _interruptIdWriteTable; /* Dummy interrupt, not used. Required by nds */
    int _interruptIdAngleTable;
    int _interruptIdMagTable;

    virtual inline void fillTable();
    virtual void freeBuffers();
};

#endif /* _sis8300llrfControlTableChannel_h */
