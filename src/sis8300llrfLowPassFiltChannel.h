/*
 * m-epics-sis8300llrf
 * Copyright (C) 2019

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrLowPassFiltChannel.h
 * @brief Header file defining the LLRF Low Pass filter class.
 * @author gabrielfedel, gabriel.fedel@esss.se
 * @date 12.8.2019
 */

#ifndef _sis8300llrfLowPassFiltChannel_h
#define _sis8300llrfLowPassFiltChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Low Pass Filter implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfLowPassFiltChannel: public sis8300llrfChannel {
public:
    sis8300llrfLowPassFiltChannel(epicsFloat64 FSampling, unsigned NearIQN);
    virtual ~sis8300llrfLowPassFiltChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setConstantA(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantA(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantB(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantB(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setCutOff(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getCutOff(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus commitParameters();

    virtual ndsStatus updateFilter();


protected:

    /* for asynReasons */
    static std::string PV_REASON_CONST_A;
    static std::string PV_REASON_CONST_B;
    static std::string PV_REASON_EN;
    static std::string PV_REASON_CUTOFF;

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);

    epicsFloat64 _cutOff;
    int _interruptIDCutOff;
    epicsInt32  _cutOffChanged;
    epicsFloat64 _FSampling;
    unsigned _NearIQN;

};

#endif /* _sis8300llrfLowPassFiltChannel_h */

