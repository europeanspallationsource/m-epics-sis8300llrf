/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfIOControlChannel.h
 * @brief Header file defining the LLRF Input and Output control (VM and IQ) Channel class
 * @author urojec
 * @date 23.5.2014
 */

#ifndef _sis8300llrfVMControlChannel_h
#define _sis8300llrfVMControlChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief VM controller specific sis8300llrfIOControlChannel
 */
class sis8300llrfVMChannel: public sis8300llrfChannel {
public:
    sis8300llrfVMChannel();
    virtual ~sis8300llrfVMChannel();
    
    inline ndsStatus checkStatuses();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus getOutputMagnitudeLimitStatus(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setOutputMagnitudeLimit(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getOutputMagnitudeLimit(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setOutputMagnitudeLimitEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getOutputMagnitudeLimitEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setOutputInverseIEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getOutputInverseIEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setOutputInverseQEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getOutputInverseQEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setSwapIQEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getSwapIQQEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setPredistortEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getPredistortEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setPredistortRC00(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortRC00(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPredistortRC01(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortRC01(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPredistortRC10(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortRC10(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPredistortRC11(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortRC11(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPredistortDCOffsetI(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortDCOffsetI(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPredistortDCOffsetQ(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPredistortDCOffsetQ(asynUser *pasynUser, epicsFloat64 *value);

protected:

    /* for asynReasons */
    static std::string PV_REASON_OUTPUT_MAGNITUDE_LIMIT_STATUS;
    static std::string PV_REASON_OUTPUT_MAGNITUDE_LIMIT_VAL;
    static std::string PV_REASON_OUTPUT_MAGNITUDE_LIMIT_EN;
    static std::string PV_REASON_OUTPUT_INVERSE_I;
    static std::string PV_REASON_OUTPUT_INVERSE_Q;
    static std::string PV_REASON_SWAP_IQ;
    static std::string PV_REASON_PREDISTORT_EN;
    static std::string PV_REASON_PREDISTORT_RC00;
    static std::string PV_REASON_PREDISTORT_RC01;
    static std::string PV_REASON_PREDISTORT_RC10;
    static std::string PV_REASON_PREDISTORT_RC11;
    static std::string PV_REASON_PREDISTORT_DC_OFFSET_I;
    static std::string PV_REASON_PREDISTORT_DC_OFFSET_Q;

    int _interruptIdOutputMagnitudeLimitStatus;
    
    /* param read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
    
    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfVMControlChannel_h */
