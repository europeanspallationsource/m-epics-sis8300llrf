/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfRefCompMonitorChannel.h
 * @brief Header file defining the Monitoring for Reference Compensation 
 * @author gabrielfedel gabriel.fedel@ess.eu
 * @date 28.05.2020
 */

#ifndef _sis8300llrfRefCompMonitorChannel_h
#define _sis8300llrfRefCompMonitorChannel_h

#include "sis8300llrfChannel.h"
#include <math.h>
#include "sis8300AIChannel.h"

/**
 * @brief Modulator filter specific implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfRefCompMonitorChannel: public sis8300llrfChannel {
public:
    sis8300llrfRefCompMonitorChannel(sis8300AIChannel * CtrlInChannel, sis8300AIChannel * RefChannel);
    virtual ~sis8300llrfRefCompMonitorChannel();
    
    inline ndsStatus checkStatuses();
    
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);


    virtual ndsStatus updateCtrlInIQAvg();
    virtual ndsStatus updateRefIQAvg();
    virtual ndsStatus getCtrlInIAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getCtrlInQAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getRefIAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getRefQAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getCtrlInMagAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getCtrlInAngAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getRefMagAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getRefAngAvg(
                asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus setAveragePos(
                asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAveragePos(
                asynUser *pasynUser,  epicsInt32 *value);
    virtual ndsStatus setAverageWidth(
                asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAverageWidth(
                asynUser *pasynUser,  epicsInt32 *value);

protected:
    
    /* for asynReasons */
    static std::string PV_REASON_CTRLIN_I_AVG;
    static std::string PV_REASON_CTRLIN_Q_AVG;
    static std::string PV_REASON_REF_I_AVG;
    static std::string PV_REASON_REF_Q_AVG;
    static std::string PV_REASON_CTRLIN_MAG_AVG;
    static std::string PV_REASON_CTRLIN_ANG_AVG;
    static std::string PV_REASON_REF_MAG_AVG;
    static std::string PV_REASON_REF_ANG_AVG;
    static std::string PV_REASON_AVG_POS;
    static std::string PV_REASON_AVG_WIDTH;

    int _interruptIdCtrlInIAvg;
    int _interruptIdCtrlInQAvg;
    int _interruptIdRefIAvg;
    int _interruptIdRefQAvg;
    int _interruptIdCtrlInMagAvg;
    int _interruptIdCtrlInAngAvg;
    int _interruptIdRefMagAvg;
    int _interruptIdRefAngAvg;

    epicsFloat64 _CtrlInI;
    epicsFloat64 _CtrlInQ;
    epicsFloat64 _RefI;
    epicsFloat64 _RefQ;
    epicsFloat64 _CtrlInMag;
    epicsFloat64 _CtrlInAng;
    epicsFloat64 _RefMag;
    epicsFloat64 _RefAng;

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);

    /*This variables is used to access calibration values from AI channels
     * so the same calibration will be used on monitoring values*/
    sis8300AIChannel * _CtrlInChannel;
    sis8300AIChannel * _RefChannel;
};

#endif /* _sis8300llrfRefCompMonitorChannel_h */

