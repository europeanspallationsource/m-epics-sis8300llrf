/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannel.h
 * @brief Header file defining the LLRF PI channel class that handles I 
 * and Q controller settings
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.5.2014
 */

#ifndef _sis8300llrfPIChannel_h
#define _sis8300llrfPIChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports PI
 *           channel
 */
class sis8300llrfPIChannel: public sis8300llrfChannel {
public:
    sis8300llrfPIChannel(sis8300llrfdrv_pi_type piType);
    virtual ~sis8300llrfPIChannel();

    sis8300llrfdrv_pi_type getPIType();

    inline ndsStatus checkStatuses();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setPIGainK(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPIGainK(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPIGainTsDivTi(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPIGainTsDivTi(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPISaturationMax(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPISaturationMax(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPISaturationMin(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPISaturationMin(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPIFixedSPVal(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPIFixedSPVal(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPIFixedFFVal(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPIFixedFFVal(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPIFixedSPEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getPIFixedSPEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setPIFixedFFEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getPIFixedFFEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getPIOverflowStatus(
                        asynUser *pasynUser, epicsInt32 *value);
protected:
    sis8300llrfdrv_pi_type        _PIType;       /**< PI type, either I 
                                                   * or Q */
    sis8300llrfdrv_gen_status_bit _GenStatusBit; /**< PI General Status 
                                                   * register bit for 
                                                   * overflow */
    /* for asynReasons */
    static std::string PV_REASON_PI_GAIN_K;
    static std::string PV_REASON_PI_GAIN_TS_DIV_TI;
    static std::string PV_REASON_SAT_MAX;
    static std::string PV_REASON_SAT_MIN;
    static std::string PV_REASON_FIXED_SP_VAL;
    static std::string PV_REASON_FIXED_FF_VAL;
    static std::string PV_REASON_FIXED_SP_ENABLE;
    static std::string PV_REASON_FIXED_FF_ENABLE;
    static std::string PV_REASON_PI_OVERFLOW_STATUS;

    int _interruptIdPIOverflowStatus;
   
    /* read/write parameter */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);

    /* state transitions */
    virtual ndsStatus onEnterReset();
};

#endif /* _sis8300llrfPIChannel_h */
