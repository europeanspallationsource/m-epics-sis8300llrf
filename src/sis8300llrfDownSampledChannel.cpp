/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDownSampledChannel.cpp
 * @brief File defining the LLRF DownSampled channel class that handles
 * downsampled values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 31.3.2020
 * 
 * TODO: add long description
 */
#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfDownSampledChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfDownSampledChannel::
                    PV_REASON_DWNSMPL_DAQ_FMT         = "DAQFormat";

/**
 * @brief DownSampled channel constructor.
 */
sis8300llrfDownSampledChannel::sis8300llrfDownSampledChannel(epicsFloat64 FSampling, sis8300AIChannel * RawChannel, epicsInt32 nearIqN) :
        sis8300llrfAuxChannel(
            FSampling,
            nearIqN,
            0) { //DownSampled
    _RawChannel = RawChannel;
}


/**
 * @brief Read all the downsampled data from memory an push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the downsampled data, convert to float and push
 * the the PV.
 *
 * */
ndsStatus sis8300llrfDownSampledChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
   
    int status, iterSrc, iterDest;
    unsigned nsamples;
    epicsInt16 * rawDataI16;
    epicsFloat64       *IMagData; /**< waveform with I or Magnitude*/
    epicsFloat64       *QAngData; /**< waveform with Q or Angle */
    epicsFloat64 ConvFactIMag, ConvFactQAng, IVal, QVal;
    epicsInt32 daq_fmt;
    double param;

    if (_RawChannel == NULL)
        return ndsError;

    if (sis8300llrfAuxChannel::onLeaveProcessing(from, to) == ndsError)
        return ndsError;
    
    if (_SamplesAcquired == 0) {
        return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesAcquired from %d", _SamplesAcquired);
    nsamples = ROUNDUP_TWOHEX(_SamplesAcquired);
    NDS_DBG("_SamplesAcquired rounded to %u", nsamples);

    rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2]; //*2 because is 32bits per sample
    IMagData = new (std::nothrow) epicsFloat64[_SamplesAcquired];
    QAngData = new (std::nothrow) epicsFloat64[_SamplesAcquired];

    if (!rawDataI16 || !IMagData || !QAngData) 
        return ndsError;


    status = sis8300llrfdrv_read_aux_channel(
                _DeviceUser, getChannelNumber(), _DownOrIntern, rawDataI16, 
                nsamples);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_aux_channel", status);

    //get dat format - 0: MA / 1: IQ / 2: DC
    readParameter(aux_param_daq_fmt, &param);
    daq_fmt = (epicsInt32) param;

    switch (daq_fmt) {
        case 1: //IQ - no calibration
            ConvFactIMag = ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_IQ_sample.frac_bits_n);
            for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                IMagData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFactIMag);
                QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactIMag);
            }
            doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        break;
        case 2: //DC - just component I has data, and could be calibrated
            ConvFactIMag = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_raw_sample.frac_bits_n);
            if (_RawChannel != NULL && _RawChannel->isCalibrationEnable()) //with calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) 
                {
                    IVal = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFactIMag);
                    _RawChannel->calibrateValue(&IVal);
                    IMagData[iterDest] = IVal;
                }
            else { // no calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) 
                    IMagData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFactIMag);
                
            }
            doCallbacksFloat64Array(NULL, 0, _interruptIdAUXQAng);
        break;
        case 0:
        default: //MA - just magnitude could be calibrated
            ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_ang_sample.frac_bits_n);
            if (_RawChannel != NULL && _RawChannel->isCalibrationEnable()) //with calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)rawDataI16[iterSrc], 
                            sis8300llrfdrv_Qmn_down_mag_sample, &IVal);
                    QVal = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactQAng);
                    _RawChannel->calibrateValue(&IVal); // Just MAG is calibrated
                    IMagData[iterDest] = IVal;
                    QAngData[iterDest] = QVal;
                }
            else {// no calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)rawDataI16[iterSrc], 
                            sis8300llrfdrv_Qmn_down_mag_sample, &IMagData[iterDest]);
                    QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactQAng);
                }
            }
            doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        break;
    }
    doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);

    delete [] rawDataI16;
    delete [] IMagData;
    delete [] QAngData;

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfDownSampledChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    /* RMS related */

    NDS_PV_REGISTER_INT32(
           sis8300llrfDownSampledChannel::PV_REASON_DWNSMPL_DAQ_FMT,
           &sis8300llrfDownSampledChannel::setDAQFormat,
           &sis8300llrfDownSampledChannel::getDAQFormat,
           &_interruptIds[aux_param_daq_fmt]
     );

    return sis8300llrfAuxChannel::registerHandlers(pvContainers);
}

/**
 * @brief Set the downsampled channel acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Format:     
 * 0 : Mag/Ang
 * 1 : IQ
 * 2 : DC
 * 3 : undefined (Mag/Ang)
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfDownSampledChannel::setDAQFormat(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value > 2)
        return ndsError;

    _ParamVals[aux_param_daq_fmt] = (int) value;

    _ParamChanges[aux_param_daq_fmt] = 1;
    return commitParameters();
}
/**
 * @brief Check the acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Format:
 * 0 : Mag/Ang
 * 1 : IQ
 * 2 : DC
 * 3 : undefined (Mag/Ang)
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfDownSampledChannel::getDAQFormat(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_daq_fmt];

    return ndsSuccess;
}


