/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDownSampledChannel.h
 * @brief Header file defining the LLRF DownSampled channel class that handles
 * downsampled values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 31.3.2020
 */

#ifndef _sis8300llrfDownSampledChannel_h
#define _sis8300llrfDownSampledChannel_h

#include "sis8300llrfAuxChannel.h"
#include "sis8300AIChannel.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports 
 *  DownSampled channel
 */
class sis8300llrfDownSampledChannel: public sis8300llrfAuxChannel {
public:
    sis8300llrfDownSampledChannel(epicsFloat64 FSampling, sis8300AIChannel * RawChannel, epicsInt32 nearIqN);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setDAQFormat(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getDAQFormat(
                        asynUser *pasynUser, epicsInt32 *value);
protected:
    /*This variable is used to access calibration values from Raw channel
     * so the same calibration will be used on downsampled values*/
    sis8300AIChannel * _RawChannel;

    static std::string PV_REASON_DWNSMPL_DAQ_FMT;

    virtual ndsStatus onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfDownSampledChannel_h */
