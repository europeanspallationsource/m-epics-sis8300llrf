/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDevice.h
 * @brief Header file defining the LLRF device class.
 * @author urojec
 * @date 23.5.2014
 */

#ifndef _sis8300llrfDevice_h
#define _sis8300llrfDevice_h

#include "ndsManager.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"

#include "sis8300llrfdrv_special_operation.h"

#include "sis8300Device.h"

#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfControlTableChannelGroup.h"
#include "sis8300AOChannelGroup.h"
#include "sis8300llrfAIChannelGroup.h"
#include "sis8300llrfAuxChannelGroup.h"
#include "sis8300RegisterChannelGroup.h"
#include "sis8300llrfAIChannel.h"

/* 
 * separating this into separte reasons - USER_IRQ and ARM, so that we can use it with dod.
     * two reasosns:
     *          1. If not, the update is faster than 14 Hz and DOD fails, because we clk everything to 14 hz
     *          2. It is really nice, because we can only readout the timestamp waveforms from the DOD. 
     *             Without the actual value, than all the events are just a waveform of timestamps when they occured
     * 
     * We don;t need separate ones for PMS and PULSE DONE - if we receive a PMS the device will go into error, and interrupts will stop 
     * comming. IT is enough to monitor the USER IRQ PV - the state will not change that much. 
     * THIS ONE IS VERY HANDY FOR DEBUGGING */
#define SIS8300LLRFNDS_STATUS_CLEAR      0x0
#define SIS8300LLRFNDS_STATUS_PULSE_DONE 0x1
#define SIS8300LLRFNDS_STATUS_PMS        0x2
#define SIS8300LLRFNDS_STATUS_ARMED      0x3

#define SIS8300LLRFNDS_ROUNDUP_EIGHT(val) (((unsigned)(val) + 0x7) & ~0x7) /**< Custom logic samples are 32 bytes,
                                                                             *  board wants 256 bit chunks for mem read */
#define SIS8300LLRFNDS_CHAN_GROUP_NUM     6
#define SIS8300LLRFNDS_SPCG_NAME          "sp"   /**< ChanelGroup name for SP table channels. */
#define SIS8300LLRFNDS_FFCG_NAME          "ff"   /**< ChanelGroup name for FF table channels. */
#define SIS8300LLRFNDS_CONTROLLER_CG_NAME "ctrl" /**< ChanelGroup name for Input/Output (IQ sampling and VM) channels. */
#define SIS8300LLRFNDS_SIGMON_CG_NAME     "sigmon"
#define SIS8300LLRFNDS_REFCOMP_CG_NAME    "refcomp"
#define SIS8300LLRFNDS_AUX_CG_NAME        "aux"


#define SIS8300LLRFNDS_PI_I_CHAN_ADDR             0
#define SIS8300LLRFNDS_PI_Q_CHAN_ADDR             1
#define SIS8300LLRFNDS_IQ_CHAN_ADDR               2
#define SIS8300LLRFNDS_VM_CHAN_ADDR               3
#define SIS8300LLRFNDS_MOD_RIPPLE_FIL_CHAN_ADDR   4
#define SIS8300LLRFNDS_ILOCK_CHAN0_ADDR           5

#define SIS8300LLRFNDS_IRQ_WAIT_TIME 0

#define SIS8300LLRFDRV_VERSION_MAJOR_KU		3 /* Major number of 3 for KU board firmware */



class sis8300llrfDevice: public sis8300Device {

public:
    sis8300llrfDevice(const std::string& name);
    virtual ~sis8300llrfDevice();

    virtual ndsStatus createStructure(const char* portName, const char* params);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setPulseType(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getPulseType(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setOperatingMode(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getOperatingMode(asynUser *pasynUser, epicsInt32 *value);
	virtual ndsStatus forceTrigger(asynUser *pasynUser, epicsInt32 value);
	virtual ndsStatus setSetupActive(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getSetupActive(asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus handleResetMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleOffMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleInitMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleOnMsg(asynUser *pasynUser, const nds::Message& value);
    
    virtual ndsStatus getUpdateReason(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getFSampling(asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getFSystem(asynUser *pasynUser, epicsFloat64 *value);
protected:
    nds::ThreadTask *_ControlLoopTask;              /**< NDS task/thread that waits for acquisition to finish. */
    ndsStatus controlLoopTask(nds::TaskServiceBase &service);
    ndsStatus check_fw_version(unsigned fwMajor, unsigned fwMinor, unsigned fwPatch);

	epicsInt32 _UpdateReason;						/**<Update Reason - indicates weather new paramters or tables were written and have to be reloaded by the controller */
    epicsInt32 _MaxPulseType;                       /**<The higest allowed pulse type */
    epicsInt32 _PulseType;                          /**<Currently selected pulse type */
    int        _PulseTypeChanged;                   /**<Paramter to track pusle type changes */
    unsigned   _PulseDoneCount;                     /**<Number of pulse done interrupts, since the last INIT->ON transition */
    unsigned   _PMSAct;                             /**<PMS status active or not */
    sis8300llrfdrv_operating_mode _OperatingMode;   /**<Currently selecetd operatin mode */
    epicsInt32 _SetupActive;                        /**<If setup is in progress */
    epicsFloat64 _FSampling;
    epicsFloat64 _FSystem;
    unsigned    _NearIQM;
    unsigned    _NearIQN;

    static std::string PV_REASON_OPERATING_MODE;
    static std::string PV_REASON_PULSE_TYPE;
    static std::string PV_REASON_PULSE_DONE_COUNT;
    static std::string PV_REASON_PULSE_MISSED;
    static std::string PV_REASON_UPDATE_REASON;
    static std::string PV_REASON_STATUS;  
    static std::string PV_REASON_ARM;
    static std::string PV_REASON_PMS_ACT;
    static std::string PV_REASON_PULSE_DONE;
    static std::string PV_REASON_FORCE_TRIGG;
    static std::string PV_REASON_SETUP_ACTIVE;
    static std::string PV_REASON_SIGNAL_ACTIVE;
    static std::string PV_REASON_F_SAMPLING;
    static std::string PV_REASON_F_SYSTEM;
    static std::string PV_REASON_ILOCK_LPS;

    int _interruptIdOperatingMode;
    int _interruptIdPulseType;
    int _interruptIdPulseDoneCount;
    int _interruptIdPulseMissed;
    int _interruptIdUpdateReason;
    int _interruptIdArm;
    int _interruptIdPmsAct;
    int _interruptIdPulseDone;
    int _interruptIdStatus;
    int _interruptIdForceTrigg;
    int _interruptIdSetupActive;
    int _interruptIdSignalActive;
    int _interruptIdFSampling;
    int _interruptIdFSystem;
    int _interruptIdILockLps;

    /* Functions to create channel groups , channels, and register the channels with channel groups */
    virtual ndsStatus registerAIChannels();
    virtual ndsStatus registerControllerChannels();
    virtual ndsStatus registerControlTableChannels(sis8300llrfdrv_ctrl_table ctrlTableType, int pulseTypes);
    virtual ndsStatus registerSignalMonitorChannels();
    virtual ndsStatus registerRefCompMonitorChannels();
    virtual ndsStatus registerAuxChannels();

    /* Handlers that we need and that the parent does not have */
    virtual ndsStatus onSwitchOn(nds::DeviceStates from, nds::DeviceStates to);  /* request INIT->ON */
    virtual ndsStatus onEnterOn(nds::DeviceStates from, nds::DeviceStates to);   /* enter ON */
    virtual ndsStatus onLeaveOn(nds::DeviceStates from, nds::DeviceStates to);   /* leave ON */
    virtual ndsStatus onEnterOffState(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus onEnterError(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus onEnterReset(nds::DeviceStates from, nds::DeviceStates to);
    virtual ndsStatus llrfCgStateRequest(nds::ChannelStates state);
    virtual ndsStatus llrfCgStateSet(nds::ChannelStates state);
    virtual ndsStatus checkPms();

    /* parents handlers */
    virtual ndsStatus onSwitchInit(nds::DeviceStates from, nds::DeviceStates to); /* request OFF->INIT, equivalent of parent INIT->ON */
    virtual ndsStatus onEnterInit(nds::DeviceStates from, nds::DeviceStates to); /* enter INIT */
    virtual ndsStatus fastInit(nds::DeviceStates from, nds::DeviceStates to);

    virtual ndsStatus commitPulseType(int pulseType);

private:
    sis8300llrfAIChannelGroup           *_CgAI;
    sis8300llrfControllerChannelGroup   *_CgCtrl;
    sis8300llrfControlTableChannelGroup *_CgSP;
    sis8300llrfControlTableChannelGroup *_CgFF;
    sis8300llrfChannelGroup             *_CgSigmon;
    sis8300llrfChannelGroup             *_CgRefCompMon;
    sis8300RegisterChannelGroup         *_CgReg;
    sis8300llrfAuxChannelGroup          *_CgAux;

    sis8300llrfChannelGroup* _llrfChannelGroups[SIS8300LLRFNDS_CHAN_GROUP_NUM]; /**<array of all LLRF specific channel groups */
    sis8300AIChannel * _AIChannels[SIS8300DRV_NUM_AI_CHANNELS];

    inline void logGenericPredefinedRegisters();
    inline ndsStatus startAllGroups();
    inline ndsStatus stopAllGroups();
    
    inline ndsStatus checkSignalStatus();
};

#endif /* _sis8300llrfDevice_h */
