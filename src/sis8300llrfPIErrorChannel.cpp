/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIErrorChannel.cpp
 * @brief File defining the LLRF PIError channel class that handles
 * PI error waveform, their settings and statistics
 * @author gabriel.fedel@ess.eu
 * @date 9.4.2020
 *
 * The class also reads out data that changes on 
 * pulse-to-pulse basis, which includes: PI error waveform and PI 
 * overflow status. This is done in the #onLeaveProcessing function.

 * 
 * RMS statistics:
 * Apart from device related settings and readouts, this class also 
 * tracks the RMS values of the PI error waveform that is recieved for 
 * each pulse and does a cumulative average. The average is reset when 
 * ANY (not just related to this class) parameters change, which is 
 * determined from the update reason in #onEneterProcessing. The
 * RMS averages can also be reset on demand.

 */
#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfPIErrorChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfPIErrorChannel::
                    PV_REASON_PIERROR_DAQ_FMT      = "DAQFormat";

std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_CURRENT_I        = "RMSCurrentI";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_CURRENT_Q        = "RMSCurrentQ";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_SMNM_IGNORE    = "RMSSMNMIgnore";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_AVERAGE_I        = "RMSAverageI";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_AVERAGE_Q        = "RMSAverageQ";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_MAX_I            = "RMSMaxI";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_MAX_Q            = "RMSMaxQ";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_PULSECNT       = "RMSPulseCnt";
std::string sis8300llrfPIErrorChannel::
                    PV_REASON_RMS_RESET          = "RMSReset";

/**
 * @brief PIError channel constructor.
 */
sis8300llrfPIErrorChannel::sis8300llrfPIErrorChannel(epicsFloat64 FSampling, epicsInt32 nearIqN) :
        sis8300llrfAuxChannel(
            FSampling,
            nearIqN,
            1) { //Internal

    registerOnEnterStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfPIErrorChannel::onEnterProcessing, 
        this, _1, _2));

    _RMSNsamplesIgnore = 0;
    _RMSAverageI        = 0.0;
    _RMSAverageQ        = 0.0;
    _RMSMaxI            = 0.0;
    _RMSMaxQ            = 0.0;
    _RMSPulseCnt       = 0;
    _RMSReset          = 0;
    _RMSCurrentI        = 0.0;
    _RMSCurrentQ        = 0.0;
}

/**
 * @see   sis8300llrfChannel:onEnterReset
 * @brief Aditianaly to parent, also reset the RMS values and check the
 *        PI overflow status
 */
ndsStatus sis8300llrfPIErrorChannel::onEnterReset() {
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }
    
    if (setRMSReset(NULL, 1) != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}


/**
 * @see   sis8300llrfChannel:onEnterProcessing
 * @brief Aditionaly to parent, also reset the RMS values if the 
 *        parameters have changed (=if the update reason was non-zero)
 */
ndsStatus sis8300llrfPIErrorChannel::onEnterProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    epicsInt32 reason;
    
    /* we want to reset the statistics anytime a paramter changes.
     * the update reason will be non zero in that case */
     
    (dynamic_cast<sis8300llrfDevice *>(_device))
                                    ->getUpdateReason(NULL, &reason);
    
    return reason ? setRMSReset(NULL, 1) : ndsSuccess;
}

/**
 * @brief Read all the internal data from memory an push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the PI error data, convert to float and push
 * the the PV. It will also calculate and update the RMS PVs.
 *
 * */
ndsStatus sis8300llrfPIErrorChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
  
    int status, iterSrc, iterDest;
    unsigned nsamples, nsamplesRampUp, nsamplesActive;
    epicsInt16 * rawDataI16;
    epicsFloat64       *IMagData; /**< waveform with I or Magnitude*/
    epicsFloat64       *QAngData; /**< waveform with Q or Angle */
    epicsFloat64 ConvFactIMag, ConvFactQAng, sumSqrdI, sumSqrdQ;
    epicsInt32 daq_fmt;
    double param;

    if (sis8300llrfAuxChannel::onLeaveProcessing(from, to) == ndsError)
        return ndsError;
    
    if (_SamplesAcquired == 0) {
        //TODO: FLAG THAT THERE WERE ZERO SAMPLES FOR RMS STATISTICS
        return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesAcquired from %d", _SamplesAcquired);
    nsamples = ROUNDUP_TWOHEX(_SamplesAcquired);
    NDS_DBG("_SamplesAcquired rounded to %u", nsamples);

    status = sis8300llrfdrv_get_acquired_nsamples(
                _DeviceUser, samples_cnt_pi_ramp_up_phase, &nsamplesRampUp);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_samples_cnt", status);

    status = sis8300llrfdrv_get_acquired_nsamples(
                _DeviceUser, samples_cnt_pi_active_phase, &nsamplesActive);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_samples_cnt", status);

    rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2]; //*2 because is 32bits per sample
    IMagData = new (std::nothrow) epicsFloat64[_SamplesAcquired];
    QAngData = new (std::nothrow) epicsFloat64[_SamplesAcquired];

    if (!rawDataI16 || !IMagData || !QAngData) 
        return ndsError;


    status = sis8300llrfdrv_read_aux_channel(
                _DeviceUser, 0, _DownOrIntern, rawDataI16, 
                nsamples);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_aux_channel", status);

    sumSqrdI = 0.0;
    sumSqrdQ = 0.0;

    //get dat format - 0: MA / 1: IQ / 2: DC
    readParameter(aux_param_daq_fmt, &param);
    daq_fmt = (epicsInt32) param;

    switch (daq_fmt) {
        case 0://MA 
            ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_intern_PI_err_ang_sample.frac_bits_n);
            for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)rawDataI16[iterSrc], 
                        sis8300llrfdrv_Qmn_intern_PI_err_mag_sample, &IMagData[iterDest]);
                QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactQAng);
                if (   iterDest >= (int)nsamplesRampUp 
                    && iterDest < (_SamplesAcquired - _RMSNsamplesIgnore)) {
                    sumSqrdI += IMagData[iterDest] * IMagData[iterDest];
                    sumSqrdQ += QAngData[iterDest] * QAngData[iterDest];
                }
            }
        break;
        case 1: //IQ
            ConvFactIMag = ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_intern_PI_err_IQ_sample.frac_bits_n);
            for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                IMagData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFactIMag);
                QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactIMag);
            }
            if (   iterDest >= (int)nsamplesRampUp 
                && iterDest < (_SamplesAcquired - _RMSNsamplesIgnore)) {
                sumSqrdI += IMagData[iterDest] * IMagData[iterDest];
                sumSqrdQ += QAngData[iterDest] * QAngData[iterDest];
            }
        break;
    }
    doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
    doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);

    /* THIS IS ALWAYS WRONG, nsamplesRampUp + nsamplesActive = 
     * nsamplesTotal + 1 
     * TODO: talk Fredrik
     * TODO: remove this, but right now it's extreamly annoying for 
     * development
     *
    if (nsamplesRampUp + nsamplesActive != (unsigned)_SamplesCount) {
        NDS_ERR("nsamplesRampUp(%u) + nsamplesActive(%u) != _SamplesCount(%u)",
                nsamplesRampUp, nsamplesActive, (unsigned)_SamplesCount);
    }
    */


    doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
    doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);

    sumSqrdI /= (epicsFloat64) 
        (_SamplesAcquired - _RMSNsamplesIgnore - nsamplesRampUp);
    
    sumSqrdQ /= (epicsFloat64) 
        (_SamplesAcquired - _RMSNsamplesIgnore - nsamplesRampUp);

    _RMSPulseCnt++;
    _RMSCurrentI = (epicsFloat64) sqrt((double) sumSqrdI);
    _RMSCurrentQ = (epicsFloat64) sqrt((double) sumSqrdQ);
    _RMSAverageI += (_RMSCurrentI - _RMSAverageI) / _RMSPulseCnt;
    _RMSAverageQ += (_RMSCurrentQ - _RMSAverageQ) / _RMSPulseCnt;
    _RMSMaxI = (_RMSCurrentI > _RMSMaxI) ? _RMSCurrentI : _RMSMaxI;
    _RMSMaxQ = (_RMSCurrentQ > _RMSMaxQ) ? _RMSCurrentQ : _RMSMaxQ;

   doCallbacksInt32(
    _RMSPulseCnt, _interruptIdRMSPulseCount);
    doCallbacksFloat64(
        _RMSCurrentI, _interruptIdRMSCurrentI);
    doCallbacksFloat64(
        _RMSCurrentQ, _interruptIdRMSCurrentQ);
    doCallbacksFloat64(
        _RMSAverageI, _interruptIdRMSAverageI);
    doCallbacksFloat64(
        _RMSAverageQ, _interruptIdRMSAverageQ);
    doCallbacksFloat64(
        _RMSMaxI, _interruptIdRMSMaxI);
    doCallbacksFloat64(
        _RMSMaxQ, _interruptIdRMSMaxQ);

    delete [] rawDataI16;
    delete [] IMagData;
    delete [] QAngData;

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfPIErrorChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIErrorChannel::PV_REASON_PIERROR_DAQ_FMT,
           &sis8300llrfPIErrorChannel::setDAQFormat,
           &sis8300llrfPIErrorChannel::getDAQFormat,
           &_interruptIds[aux_param_daq_fmt]
     );

    /* RMS related */

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_SMNM_IGNORE,
           &sis8300llrfPIErrorChannel::setRMSSMNMIgnore,
           &sis8300llrfPIErrorChannel::getRMSSMNMIgnore,
           &_interruptIdRMSSMNMIgnore
     );

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_CURRENT_I,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSCurrentI);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_CURRENT_Q,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSCurrentQ);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_AVERAGE_I,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSAverageI);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_AVERAGE_Q,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSAverageQ);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_MAX_I,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSMaxI);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_MAX_Q,
           &sis8300llrfPIErrorChannel::setFloat64,
           &sis8300llrfPIErrorChannel::getFloat64,
           &_interruptIdRMSMaxQ);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_PULSECNT,
           &sis8300llrfPIErrorChannel::setInt32,
           &sis8300llrfPIErrorChannel::getInt32,
           &_interruptIdRMSPulseCount);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIErrorChannel::PV_REASON_RMS_RESET,
           &sis8300llrfPIErrorChannel::setRMSReset,
           &sis8300llrfPIErrorChannel::getInt32,
           &_interruptIdRMSReset);

    return sis8300llrfAuxChannel::registerHandlers(pvContainers);
}

/**
 * @brief Set the PIError channel acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Format:     
 * 0 : Mag/Ang
 * 1 : IQ
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIErrorChannel::setDAQFormat(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value > 1)
        return ndsError;

    _ParamVals[aux_param_daq_fmt] = (int) value;

    _ParamChanges[aux_param_daq_fmt] = 1;
    return commitParameters();
}
/**
 * @brief Check the acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Format:
 * 0 : Mag/Ang
 * 1 : IQ
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfPIErrorChannel::getDAQFormat(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_daq_fmt];

    return ndsSuccess;
}


/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ============ RELATED TO PI ERROR RMS=============== */
/**
 * @brief Write 1 to reset the RMS value
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set to 1 to reset the RMS
 * 
 * @return ndsSuccess   Always
 * 
 * If the value is reset, than the RMS cumulative average will
 * be reset with the next pulse and started again
 */
ndsStatus sis8300llrfPIErrorChannel::setRMSReset(
                asynUser *pasynUser, epicsInt32 value) {

    if (value) {
        _RMSPulseCnt = 0;
        _RMSAverageI = 0.0;
        _RMSAverageQ = 0.0;
        _RMSMaxI = 0.0;
        _RMSMaxQ = 0.0;
        doCallbacksInt32(_RMSPulseCnt, _interruptIdRMSPulseCount);
        doCallbacksFloat64(_RMSAverageI, _interruptIdRMSAverageI);
        doCallbacksFloat64(_RMSAverageQ, _interruptIdRMSAverageQ);
        doCallbacksFloat64(_RMSMaxI, _interruptIdRMSMaxI);
        doCallbacksFloat64(_RMSMaxQ, _interruptIdRMSMaxQ);
    }

    return ndsSuccess;
}

/**
 * @brief Specify the number of PI erros to ignore at the end of the 
 *        acquired PI error waveform when callculating the RMS.
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Number of samples to ignore
 * 
 * @return  ndsError    If number of samples in less than zero
 * @return  ndsSuccess  All other cases
 */
ndsStatus sis8300llrfPIErrorChannel::setRMSSMNMIgnore(
                asynUser *pasynUser, epicsInt32 value) {
    if (_RMSNsamplesIgnore < 0) {
        return ndsError;
    }

    _RMSNsamplesIgnore = (int) value;

    return ndsSuccess;
}
/**
 * @brief Get the number of samples that are ignored at the end of the 
 *        acquired PI error waveform when RMS is being callculated.
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Will hold the number of ignored samples on 
 *                          success
 * 
 * @return  ndsError    If this call is made during IOC INIT
 * @return  ndsSuccess  All other cases
 */
ndsStatus sis8300llrfPIErrorChannel::getRMSSMNMIgnore(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _RMSNsamplesIgnore;

    return ndsSuccess;
}
