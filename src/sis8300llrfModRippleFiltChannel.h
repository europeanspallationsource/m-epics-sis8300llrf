/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfModRippleFiltChannel.h
 * @brief Header file defining the LLRF Modulator ripple filter class
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.1.2015
 */

#ifndef _sis8300llrfModRippleFiltChannel_h
#define _sis8300llrfModRippleFiltChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Modulator filter specific implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfModRippleFiltChannel: public sis8300llrfChannel {
public:
    sis8300llrfModRippleFiltChannel();
    virtual ~sis8300llrfModRippleFiltChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setConstantS(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantS(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantC(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantC(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setConstantA(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getConstantA(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setQEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getQEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setIEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getIEnable(
                        asynUser *pasynUser, epicsInt32 *value);

protected:

    /* for asynReasons */
    static std::string PV_REASON_CONST_S;
    static std::string PV_REASON_CONST_C;
    static std::string PV_REASON_CONST_A;
    static std::string PV_REASON_Q_EN;
    static std::string PV_REASON_I_EN;

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
};

#endif /* _sis8300llrfModRippleFiltChannel_h */

