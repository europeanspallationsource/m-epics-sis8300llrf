/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfILOCKtChannel.h
 * @brief Header file defining the LLRF Modulator ripple filter class
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.1.2015
 */

#ifndef _sis8300llrfILOCKChannel_h
#define _sis8300llrfILOCKChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Modulator filter specific implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfILOCKChannel: public sis8300llrfChannel {

public:
    sis8300llrfILOCKChannel();
    virtual ~sis8300llrfILOCKChannel();

    virtual ndsStatus initialize();

    virtual ndsStatus readParameters();
    virtual ndsStatus markAllParametersChanged();
    
    inline ndsStatus checkStatuses();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setILOCKCondition(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getILOCKCondition(
                        asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus getValueInt32(asynUser *pasynUser, epicsInt32 *value);

protected:
    unsigned                       _HarlinkNum;            /**< Harling 
                                 * input corresponding to this channel */
    sis8300llrfdrv_ilock_condition _ILOCKCondition;        /**< Interlock 
                                 * condition */
    int                            _ILOCKConditionChanged; /**< Flag for 
                                 * tracking interlock condition setting 
                                 * changes */
    
    /* asynReasons */
    static std::string PV_REASON_ILOCK_CONDITION;
    int _interruptIdILOCKCondition;

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
    
    /* write to hardware */
    virtual ndsStatus writeToHardware();
};

#endif /* _sis8300llrfILOCKChannel_h */

