###########
#### Registers
## LOG REG SCANNING, I/O Intr
#epicsEnvSet("REG_SCAN" "I/O Intr") 2

dbLoadRecords("sis8300Register.db",      "PREFIX=$(LLRF_PREFIX),ASYN_PORT=$(LLRF_PORT),REG_SCAN=2")
dbLoadRecords("sis8300llrf-Register.db", "PREFIX=$(LLRF_PREFIX),ASYN_PORT=$(LLRF_PORT),REG_SCAN=2")
