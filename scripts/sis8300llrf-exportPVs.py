#!/usr/bin/env python3

import epics
import time
import sys
import os
from datetime import datetime


#read all scalar pv values
def readAllScalarValues(filename, extraLine):
        filename.write(extraLine)
        for line in open(logListFile, 'r'):
                name = llrfprefix + ':' + line.split()[0]
                value = epics.caget(name+'.VAL', as_string=True)
                if value is None:
                    print("timeout getting pv " + name)
                    print("EXITING")
                    break
                valuesLogFile.write(name + '\t\t' + value + '\n')

#program 'entry'. get pulse number
if (len(sys.argv) < 3 or (len(sys.argv) == 2 and (sys.argv[1] == '--h' or sys.argv[1] == '-help'))):
        print('\nUSAGE: exportPVs [pv_list] [file_name] [llrf-prefix]')
        print('\nARGUMENTS:')
        print('\t[pv_list]         contains a listof PVs to dump before and after test')
        print('\t      file format:    <PV_NAME><newline>, where <PV_NAME> is without the device prefix')
        print('\t                              for exemple LLRF-LION:PT would be only PT, PVs need to be in')
        print('\t                              in a separte line each')
        print('\t[file_name]       name of the exported file')
        print('\t[llrf_prefix]     optional. Prefix of the llrf device. If none is given, the default')
        print('\t                  will be used (check with echo $SIS8300LLRF_PREFIX)')
        print('\nACTIVITIES:')
        print('\nStore all the listed pv values into a file')
        sys.exit(-1)
logListFile = sys.argv[1]
pvsFile = sys.argv[2]

if len(sys.argv) < 4:
        llrfprefix = os.getenv("SIS8300LLRF_PREFIX")
else:
        llrfprefix = sys.argv[3]


if llrfprefix is None:
    llrfprefix = "LLRF"

print('using ', llrfprefix, ' to get the parameter snapshot')

valuesLogFile = open(pvsFile, 'w')
readAllScalarValues(valuesLogFile, "")
valuesLogFile.close()

