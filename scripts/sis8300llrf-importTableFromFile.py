#!/usr/bin/env python3
import sys
import numpy
import epics
if (len(sys.argv) < 3 or (len(sys.argv) == 2 and (sys.argv[1] == '--h' or sys.argv[1] == '-help'))):
	print('\nUSAGE: sis8300llrf-demo-importTableFromFile.py [pv_name] [filename]\n')
	print('ARGUMENTS:')
	print('\t[pv_name]  table to write')
	print('\t[filename] file that has the table')
	print('ACTION:')
	print('\tThis will load the table from [filename] and write it to [pv_name]')
	print('\tThe table elements must be separated by whitespace')
	sys.exit(-1)
p = epics.PV(sys.argv[1])
p.put(numpy.loadtxt(sys.argv[2],dtype='float'))
