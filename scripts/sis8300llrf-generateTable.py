#!/usr/bin/env python3
import sys
import numpy
import epics
if (len(sys.argv) < 6 or (len(sys.argv) == 2 and (sys.argv[1] == '--h' or sys.argv[1] == '-help'))):
	print('USAGE: sis8300llrf-demo-generateTable.py [pv_name] [mode] [nelm] [arg1] [arg2] [filename]\n')
	print('ARGUMENTS:')
	print('\t[pv_name]  table to write')
	print('\t[mode]     can be fixed, ramp or sin')
	print('\t[nelm]     number of lements in the table')
	print('\t[arg1]	    if mode is fixed this is the value')
	print('\t           if mode is ramp this is the starting point')
	print('\t           if mode is sin this is the amplitude')
	print('\t[arg2]	    if mode is fixed this is meaningless')
	print('\t           if mode is ramp this is the end point')
	print('\t           if mode is sin this is the frequency')
	print('\t[filename] filename to write the table to, optional')
	print('ACTION:')
	print('\tThis will create a table and write it to the PV')
	print('\tThe first element in the table will always be 0 (see fw doc)')
	sys.exit(-1)
p = epics.PV(sys.argv[1])
mode = sys.argv[2]
steps = int(float(sys.argv[3]))
if (mode == 'fixed'):
    val = float(sys.argv[4])
    r = numpy.empty(steps)
    r.fill(val)
    r[0] = 0.0
if (mode == 'ramp'):
    start = float(sys.argv[4])
    stop = float(sys.argv[5])
    r = numpy.linspace(start, stop, steps)
if (mode == 'sin'):
    ampl = float(sys.argv[4])
    freq = float(sys.argv[5])
    t = numpy.arange(steps)
    r = ampl * numpy.sin(2 * numpy.pi * freq * t / float(steps))
    r[0] = 0.0
p.put(r)
if (len(sys.argv) == 7):
    numpy.savetxt(sys.argv[6], r, delimiter='\n')

