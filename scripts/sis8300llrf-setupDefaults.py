#!/usr/bin/env python3
import epics
import sys
import xml.etree.ElementTree as ET
import subprocess

#parse an xml file (.pvs from CSS)
# get the default PV values and write them to the device
if (len(sys.argv) < 3 or (len(sys.argv) == 2 and (sys.argv[1] == '--h' or sys.argv[1] == '-help'))):
	print('\nUSAGE: sis8300llrf-demo-setupDefaults.py [file] [file_format]\n')
	print('ARGUMENTS:')
	print('\t[filename]    file with a list of PVs and their default values')
	print('\t[file_format] 1 for a .pvs file')
	print('\t              2 for a file in format of <PV_NAME> <whitespace> <VALUE>')
	print('ACTION:')
	print('\tThis will set all the PVS listed in the [filename] to the specified values')
	sys.exit(-1)

filename = sys.argv[1]
filetype = int(sys.argv[2])

if filetype == 1:
    tree = ET.parse(filename)
    root = tree.getroot()
    pvlist=root.find('pvlist')
    for pv in pvlist.findall('pv'):
        name = pv.find('name').text
        saved_value = pv.find('saved_value').text
        #epics.caput(name+'.VAL',saved_value)
        #print name, '\t\t\tSET:', saved_value, '\tRBV:', epics.caget(name+'.VAL', as_string=True)
        bashCommand = "caput " + name + " " + saved_value
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        print(process.communicate()[0])
elif filetype == 2:
    for line in open(filename, 'r'):
        lineArray = line.split()
        if len(lineArray) > 0:
                name = lineArray[0]
                saved_value = lineArray[1]
                #   epics.caput(name+'.VAL',saved_value)
                #print name, '\t\t\tSET:', saved_value, '\tRBV:', epics.caget(name+'.VAL', as_string=True)
                bashCommand = "caput " + name + " " + saved_value
                process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                print(process.communicate()[0])
else:
    print("Wrong filetype selection")
