# m-epics-sis8300llrf

2020-09-11: This repository has been deprecated. Moved to https://gitlab.esss.lu.se/epics-modules/rf/sis8300llrf

## Configuring CS-Studio to visualize large DAQs

If you have problems to visualize large DAQs with OPIs, you must increase the Max Array Size inside CS-Studio. For this access: Edit -> Preferences -> CSS Core -> Data Source -> Channel Access -> Max Array Size [bytes].

Update the value to 8388624 (this allow to see up to 2097152 samples) or greater.
